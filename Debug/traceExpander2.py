import sys
import subprocess
import os
triggertoken = ""
timetoken = ""
state = 0

def modifyStep(step, increment):
	global triggertoken, timetoken, state
	if(state == 0):	
		p2 = step.find(triggertoken)
		if(p2 > 0):
			p1 = step.rfind("\n", 0, p2)
			if(step[p1+1 : p2] == "1"):
				state = 1
				step = step[:p1] + step[step.find("\n", p1+1):]
		p2 = step.find(timetoken)
		if(p2 > 0):
			p1 = step.rfind("\n", 0, p2)
			step = step[:p1] + step[step.find("\n", p1+1):]
	elif(state == 1):
		p2 = step.find(triggertoken)
		if(p2 > 0):
			p1 = step.rfind("\n", 0, p2)
			if(step[p1+1 : p2] == "0"):
				state = 0
				step = step[:p1] + step[step.find("\n", p1+1):]
				p2 = step.find(timetoken)
				if(p2 > 0):
					p1 = step.rfind("r", 0, p2)
					tinc = 0
					try:
						tinc = int(step[p1+1 : p2-3])
					except:
						print("Error")
						print(step)
						print(step[p1+1:p2-3])
					increment += tinc
					p1 = step.rfind("\n", 0, p1)
					p2 = step.find("\n", p1+1)
					step = step[:p1] + step[p2:]
	else:
		print("Error in state")
		state = 0
		
	p1 = step.find("#")
	p2 = step.find("\n", p1)
	time = int(step[p1+1 : p2])
	time += increment
	step = step[:p1+1] + str(time) + step[p2:]
		
	ok = 0
	p1 = step.find("\n")
	for i in range( len(step) - p1 ):
		if(step[p1+i] != "\n"):
			ok = 1
			break
	if(ok == 0):
		step = ""
	
	return step, increment

def readUntilSequence(file, sequence):
	r = ""
	ok = True
	state = 0
	while(ok):
		byte = file.read(1)
		r += byte
		if(not byte):
			ok = False
		if(byte == sequence[state]):
			state += 1
			if(state == len(sequence)):
				ok = False
		else:
			state = 0
	return r

def readUntil(file, char):
	r = ""   
	ok = True
	while(ok):
		byte = file.read(1)
		r += byte
		if(not byte or byte == char):
			ok = False
	return r

def main(argc, argv):
	global triggertoken, timetoken, state
	filename = ""
	if(argc < 2):
		print("<pdata>No input file given<\\pdata>")
		return -1
	filename = argv[1]
	pdot = filename.rfind(".", 0)

	try:
		file = open(filename, 'r')
	except:
		print("<pdata>Can not open input file<\\pdata>")
		return -1
	vcd2name = filename[:pdot] + "_2.vcd"
	file2 = open(vcd2name, 'w')
	fpos = file.tell()
	file.seek(0, os.SEEK_END)
	filesize = file.tell()
	file.seek(fpos,os.SEEK_SET)
	data = readUntil(file, "#")#file.read()
	
	outdata = ""
	outdatapos = 0;
	
	atokenp2 = data.rfind("  ", 0, data.find("tAdvance"))
	if(atokenp2 == -1):
		print("<pdata>Can not find token 'tAdvance'<\\pdata>")
		return -1
	atokenp1 = data.rfind("  ", 0, atokenp2) + 2
	triggertoken = data[atokenp1 : atokenp2]
	
	atok1 = data.rfind("\n", 0, atokenp1)
	atok2 = data.find("\n", atok1 + 1)
	
	ttokenp2 = data.rfind("  ", 0, data.find("advanceTime"))
	if(ttokenp2 == -1):
		print("<pdata>Can not find token 'advanceTime'<\\pdata>")
		return -1
	ttokenp1 = data.rfind("  ", 0, ttokenp2) + 2
	timetoken = data[ttokenp1 : ttokenp2]
	
	ttok1 = data.rfind("\n", 0, ttokenp1)
	ttok2 = data.find("\n", ttok1 + 1)
	
	if(ttok1 < atok1):
		outdata += data[outdatapos : ttok1]
		outdata += data[ttok2 : atok1]
		outdatapos = atok2
	else:
		outdata += data[outdatapos : atok1]
		outdata += data[atok2 : ttok1]
		outdatapos = ttok2
	
	tdata = (data[outdatapos : ])
	p1 = tdata.find(triggertoken)
	if(p1 > 0):
		p1 = tdata.rfind("\n", 0, p1)
		p2 = tdata.find("\n", p1+1)
		tdata = tdata [:p1] + tdata[p2:]
	p1 = tdata.find(timetoken)
	if(p1 > 0):
		p1 = tdata.rfind("\n", 0, p1)
		p2 = tdata.find("\n", p1+1)
		tdata = tdata [:p1] + tdata[p2:]
	
	outdata += tdata
	file2.write(outdata)
	outdata = ""
	outdatapos = 0 #data.find("#", outdatapos)
	data = readUntilSequence(file,triggertoken) + readUntil(file,"#")
	data = data[ : -1]
	#print(triggertoken, timetoken)
	
	p1 = data.find(triggertoken, outdatapos)
	if(p1 > 0):
		p1 = data.rfind("#", 0, p1)
		outdata += data[outdatapos : p1]
	
	file2.write(outdata)
	outdata = ""
	pct = 0
	#done = 0
	pinc = 10
	if(filesize > 1000000):
		pinc = 1
	inc = 0
	while(data != ""):
		#p2 = data.find("#", p1+1)
		
		#if(p2 > 0):
		#	d, i = modifyStep(data[p1 : ], inc)
		#	inc = i
		#	outdata += d
		#	p1 = p2
		#else: #last block
		d, i = modifyStep(data[p1 : ], inc)
		inc = i
		file2.write(d)
		p1 = 0
		data = "#" + readUntil(file, "#")
		if(data != ""):
			data = data[ : -1]
		
		fpos = file.tell()
		if(100 * fpos / filesize > pct):
			print("<pdata>" + str(pct) + "%<\\pdata>")
			pctn = int(100 * fpos / filesize)
			if(pctn - pct < pinc):
				pct += pinc
			else:
				pct = pctn


	file.close()
	file2.close()
	fstname = filename[:pdot + 1] + "fst"
	subprocess.call(["vcd2fst","-c", "-p", filename, fstname])
	
	os.remove(filename)
	fst2name = vcd2name[:vcd2name.rfind(".", 0) + 1] + "fst"
	subprocess.call(["vcd2fst","-c", "-p",  vcd2name, fst2name])
	os.remove(vcd2name)

main(len(sys.argv), sys.argv)

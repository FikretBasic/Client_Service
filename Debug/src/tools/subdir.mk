################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/tools/HelperFunctions.cpp \
../src/tools/SimplePacket.cpp \
../src/tools/Stylist.cpp 

OBJS += \
./src/tools/HelperFunctions.o \
./src/tools/SimplePacket.o \
./src/tools/Stylist.o 

CPP_DEPS += \
./src/tools/HelperFunctions.d \
./src/tools/SimplePacket.d \
./src/tools/Stylist.d 


# Each subdirectory must supply rules for building sources it contributes
src/tools/%.o: ../src/tools/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/usr/local/systemc-2.3.1a/include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



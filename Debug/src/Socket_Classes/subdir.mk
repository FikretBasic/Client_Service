################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/Socket_Classes/SocketAPI.cc 

CPP_SRCS += \
../src/Socket_Classes/NETAddr.cpp \
../src/Socket_Classes/SOCKETHandler.cpp \
../src/Socket_Classes/SOCKETLine.cpp 

CC_DEPS += \
./src/Socket_Classes/SocketAPI.d 

OBJS += \
./src/Socket_Classes/NETAddr.o \
./src/Socket_Classes/SOCKETHandler.o \
./src/Socket_Classes/SOCKETLine.o \
./src/Socket_Classes/SocketAPI.o 

CPP_DEPS += \
./src/Socket_Classes/NETAddr.d \
./src/Socket_Classes/SOCKETHandler.d \
./src/Socket_Classes/SOCKETLine.d 


# Each subdirectory must supply rules for building sources it contributes
src/Socket_Classes/%.o: ../src/Socket_Classes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/usr/local/systemc-2.3.1a/include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Socket_Classes/%.o: ../src/Socket_Classes/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/usr/local/systemc-2.3.1a/include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



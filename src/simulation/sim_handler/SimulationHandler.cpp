/*
 * SimulationHandler.cpp
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "SimulationHandler.h"

namespace simc {

SimulationHandler::SimulationHandler() : sim(NULL) {}

SimulationHandler::~SimulationHandler()
{
	delete this->sim;
}

void SimulationHandler::setSimulation(Simulation *sim)
{
	this->sim = sim;
}

const char* SimulationHandler::messageSetter(const int instructionCounter, const uint16_t connectionCode)
{
	std::string message;
	message = "";

	switch(instructionCounter)
	{
		case 0:
			message = "<t>status</t><d>ready</d>";
			break;
		case 1:
			message = "<t>code</t><d>" + Str(connectionCode) + "</d>";
			break;
		case 2:
			message = "<t>status</t><d>command_received</d>";
			break;
		case 3:
			message = "<t>status</t><d>sim_started</d>";
			break;
		case 4:
			message = "<t>status</t><d>sim_finished</d>";
			break;
		case 5:
			message = "<t>result</t><d>" + Str("Result") + "</d>";
			break;
		case -1:
			message = "<t>status</t><d>connection_end</d>";
			break;
		default:
			message = "<t>status</t><d>ready</d>";
	}

	return message.c_str();
}

int SimulationHandler::messageInterpreter(char* message)
{
	SimplePacket packet(message);

	if (packet.getType().compare("status") == 0)
	{
		if (packet.getData().compare("code_request") == 0)
		{
			return 1;
		}
		else if (packet.getData().compare("code_passed") == 0)
		{
			return 0;
		}
		else if (packet.getData().compare("code_failed") == 0)
		{
			return -1;
		}
		else if (packet.getData().compare("sim_start") == 0)
		{
			return 3;
		}
		else if (packet.getData().compare("sim_status") == 0)
		{
			return 4;
		}
		else if (packet.getData().compare("get_results") == 0)
		{
			return 5;
		}
		else if (packet.getData().compare("waiting") == 0)
		{
			return 0;
		}
		else if (packet.getData().compare("connection_end") == 0)
		{
			return -1;
		}
	}
	else if(packet.getType().compare("command") == 0)
	{
		return this->applySimulationCommands(packet.getData());
	}

	return 0;
}

}

/*
 * SmartSensorHandler.cpp
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "SmartSensorHandler.h"

namespace simc {

SmartSensorHandler::SmartSensorHandler() : runtime(0) {}

SmartSensorHandler::~SmartSensorHandler()
{
	std::cout << "<rtime>" << runtime << "<\\rtime>" << std::endl;
}

void SmartSensorHandler::setSimulation(Simulation *sim)
{
	this->sim = sim;
}

int SmartSensorHandler::applySimulationCommands(std::string data)
{
	std::string ex[3] = {bufnames[3], bufnames[5], bufnames[6]};
	pthread_mutex_lock(&bufUse);
	clearBufferValues(&buffer, ex, 3);
	pthread_mutex_unlock(&bufUse);

	std::cout << "<ddata>" << data << "<\\ddata>" << std::endl;
	processInput(data);

	std::cout << "<ddata>";
	pthread_mutex_lock(&bufUse);
	printBufferContents(&buffer);
	pthread_mutex_unlock(&bufUse);
	std::cout << "<\\ddata>";

	// ----------------------------------

	pthread_mutex_lock(&bufUse);
	if(getNumValuesOf(&buffer, bufnames[1]) > 0)
	{
		std::string unit = getBufferValue(&buffer, bufnames[1]);
		clearBufferValue(&buffer, bufnames[1]);
		if(unit == "FS")
			stepUnit = SC_FS;
		else if(unit == "PS")
			stepUnit = SC_PS;
		else if(unit == "NS")
			stepUnit = SC_NS;
		else if(unit == "US")
			stepUnit = SC_US;
		else if(unit == "MS")
			stepUnit = SC_MS;
		else if(unit == "SEC")
			stepUnit = SC_SEC;
		else
			stepUnit = SC_NS;
		//std::cout << "<ddata>time unit " << stepUnit << "<\\ddata>" << std::endl;
	}

	if(getNumValuesOf(&buffer, bufnames[2]) > 0)
	{
		try
		{
			stepSize = atoi( (getBufferValue(&buffer, bufnames[2])).c_str() );
		}
		catch(int e)
		{
			std::cout << "<ddata>StepSizeException " << e << "<\\ddata>" << std::endl;
			stepSize = 0;
		}
		clearBufferValue(&buffer, bufnames[2]);
		//std::cout << "<ddata>step size " << stepSize << "<\\ddata>" << std::endl;
	}
	//done by the antenna
	//while(getNumValuesOf(&buffer, bufnames[3]) > 0) //commands
	//{

		//std::string cmd = getBufferValue(&buffer, bufnames[3]);
		//clearBufferValue(&buffer,bufnames[3]);
		//pSensor->pAntenna->receiving = cmd;
		//printDebug(cmd);
	//}

	while(getNumValuesOf(&buffer, bufnames[4]) > 0) //sensor data
	{
		std::string data = getBufferValue(&buffer, bufnames[4]);
		clearBufferValue(&buffer, bufnames[4]);
		size_t begin, end;
		begin = 0;
		end  = data.find(" ", begin);
		while(end != std::string::npos)
		{
			unsigned long c = strtoul(data.substr(begin, end).c_str(), NULL, 10);

			std::string tempC = Str(c);
			this->sim->setSimulationParameter(tempC, 4);

			//this->sim->getSensor()->Gen->customData.nb_write(c);

			begin = end + 1;
			end = data.find(" ", begin);
		}
		unsigned long c = strtoul(data.substr(begin).c_str(), NULL, 10);
		std::string tempC = Str(c);
		this->sim->setSimulationParameter(tempC, 4);

		//this->sim->getSensor()->Gen->customData.nb_write(c);

		//pSensor->pGenerator->customData.nb_write(c);
	}

	//done by the antenna
	while(getNumValuesOf(&buffer,bufnames[6]) > 0) //transmitted energy
	{
		std::string data = getBufferValue(&buffer, bufnames[6]);
		unsigned long c = strtoul(data.c_str(), NULL, 10);

		std::string tempC = Str(c);
		this->sim->setSimulationParameter(tempC, 6);

		//this->sim->getSensor()->Ant->receivingEnergy = (double)c;

	//	double en = (double)c / 3000.0;
	//	if(c > 4)
	//	{
	//		pSensor->pAntenna->receivingEnergy = en;
	//	}
	//	else
	//	{
	//		pSensor->pAntenna->receivingEnergy = 0;
	//	}
		clearBufferValue(&buffer, bufnames[6]);
	}

	std::cout << getBufferValue(&buffer, bufnames[5]) << std::endl;

	std::string ret = gazebo::getBufferValue(&buffer, bufnames[5]);
	gazebo::clearBufferValue(&buffer, bufnames[5]);

	pthread_mutex_unlock(&bufUse);

	if(ret == "1")
	{
		return 3;
	}
	else if (ret == "0")
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

void SmartSensorHandler::startSimulation()
{
	//this->sim->getSensor()->Adv->nextStop = sc_time_stamp() + sc_time(stepSize, stepUnit);
	this->sim->setSimulationParameter("", 0);
	sc_start(stepSize, stepUnit);

	switch (stepUnit)
	{
		case SC_SEC:
			runtime += stepSize;
			break;
		case SC_MS:
			runtime += (stepSize/1000.0);
			break;
		case SC_US:
			runtime += (stepSize/1000000.0);
			break;
		case SC_NS:
			runtime += (stepSize/1000000000.0);
			break;
		case SC_PS:
			runtime += (stepSize/1000000000000.0);
			break;
		case SC_FS:
			runtime += (stepSize/1000000000000000.0);
			break;
		default:
			//std::cerr << "Wrong time Unit" << std::endl; // should not happen
			assert(false);
			break;
	}
}

void SmartSensorHandler::processInput(std::string in)
{
	pthread_mutex_lock(&bufUse);

	searchTokenAndAppend(&buffer, bufTokens[0], bufnames[0], in);
	searchTokenAndAppend(&buffer, bufTokens[1], bufnames[1], in);
	searchTokenAndAppend(&buffer, bufTokens[2], bufnames[2], in);
	searchTokenAndAppend(&buffer, bufTokens[3], bufnames[3], in);
	searchTokenAndAppend(&buffer, bufTokens[4], bufnames[4], in);
	searchTokenAndAppend(&buffer, bufTokens[5], bufnames[5], in);
	searchTokenAndAppend(&buffer, bufTokens[6], bufnames[6], in);

	pthread_mutex_unlock(&bufUse);
}

std::string SmartSensorHandler::lockedGetAndRemove(std::string name, int value)
{

	pthread_mutex_lock(&bufUse);
	std::string ret = gazebo::getBufferValue(&buffer, name, value);
	gazebo::clearBufferValue(&buffer, name, value);
	//std::string ret = gazebo::getAndRemoveValue(&buffer, name, value);
	//std::cout << "<ddata>" << name << ": " << ret << "<\\ddata>\n";
	pthread_mutex_unlock(&bufUse);
	return ret;
}

} /* namespace simc */

/*
 * SimulationHandler.h
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "../../Includes.h"
#include "../Simulation.h"

#ifndef SIMULATION_SIM_HANDLER_SIMULATIONHANDLER_H_
#define SIMULATION_SIM_HANDLER_SIMULATIONHANDLER_H_

namespace simc {

class SimulationHandler
{
public:
	SimulationHandler();
	virtual ~SimulationHandler();

	const char* messageSetter(const int instructionCounter, const uint16_t connectionCode = 0);
	int messageInterpreter(char* message);

	void setSimulation(Simulation *sim);

	virtual int applySimulationCommands(std::string data) = 0;
	virtual void startSimulation() = 0;

	Simulation *sim;
};

} /* namespace simc */

#endif /* SIMULATION_SIM_HANDLER_SIMULATIONHANDLER_H_ */

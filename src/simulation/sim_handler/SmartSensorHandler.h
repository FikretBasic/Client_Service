/*
 * SmartSensorHandler.h
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "SimulationHandler.h"
#include "../SmartSensorTop.h"
#include "../smart_sensor/includes.h"
#include "../smart_sensor/utils.h"

#ifndef SIMULATION_SIM_HANDLER_SMARTSENSORHANDLER_H_
#define SIMULATION_SIM_HANDLER_SMARTSENSORHANDLER_H_

namespace simc {

class SmartSensorHandler : public SimulationHandler
{
public:
	SmartSensorHandler();
	virtual ~SmartSensorHandler();

	void setSimulation(Simulation *sim);
	int applySimulationCommands(std::string data);
	void startSimulation();

	std::string lockedGetAndRemove(std::string name, int value = 0);

	void processInput(std::string in);

private:
	double runtime;
};

} /* namespace simc */

#endif /* SIMULATION_SIM_HANDLER_SMARTSENSORHANDLER_H_ */

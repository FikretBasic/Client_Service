/*
 * CounterHandler.cpp
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "CounterHandler.h"

namespace simc {

CounterHandler::CounterHandler() : stepUnit(SC_NS), stepSize(100) {}

CounterHandler::~CounterHandler() {}

int CounterHandler::applySimulationCommands(std::string data)
{

	int commandPos = 0;
	std::string handledComm = handleCommand(data, commandPos);

	if (handledComm != "")
	{
		if (commandPos == 0)
		{
			if(handledComm == "FS")
				stepUnit = SC_FS;
			else if(handledComm == "PS")
				stepUnit = SC_PS;
			else if(handledComm == "NS")
				stepUnit = SC_NS;
			else if(handledComm == "US")
				stepUnit = SC_US;
			else if(handledComm == "MS")
				stepUnit = SC_MS;
			else if(handledComm == "SEC")
				stepUnit = SC_SEC;
			else
				stepUnit = SC_NS;
		}
		else if (commandPos == 1)
		{
			stepSize = atoi(handledComm.c_str());
		}
		else
		{
			if (handledComm.compare("1") == 0)
				return 3;
			else
				return -1;
		}
	}
	else
	{
		return 0;
	}

	return 0;
}

void CounterHandler::startSimulation()
{
	sc_start(this->stepSize, this->stepUnit);
}

std::string CounterHandler::handleCommand(const std::string &command, int &commandPos)
{
	size_t numOfTokens = 3;
	std::string bufferTokens[3] = {"TU", "TS", "RUN"};

	for (size_t i = 0; i < numOfTokens; i++)
	{
		if (get_str_between_two_str(command, "<", ">").compare(bufferTokens[i]) == 0)
		{
			commandPos = i;
			return get_str_between_two_str(command, "<" + bufferTokens[i] + ">", "<\\" + bufferTokens[i] + ">");
		}
	}

	return "";
}

} /* namespace simc */

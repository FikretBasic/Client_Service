/*
 * CounterHandler.h
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "SimulationHandler.h"
#include "../CounterTop.h"

#ifndef SIMULATION_SIM_HANDLER_COUNTERHANDLER_H_
#define SIMULATION_SIM_HANDLER_COUNTERHANDLER_H_

namespace simc {

class CounterHandler : public SimulationHandler
{
public:
	CounterHandler();
	virtual ~CounterHandler();

	void setSimulation(Simulation *sim);
	int applySimulationCommands(std::string data);
	void startSimulation();

	std::string handleCommand(const std::string &command, int &commandPos);

private:
	sc_time_unit stepUnit;
	int stepSize;
};

} /* namespace simc */

#endif /* SIMULATION_SIM_HANDLER_COUNTERHANDLER_H_ */

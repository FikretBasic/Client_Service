/*
 * Simulation.h
 *
 *  Created on: 21 Mar 2018
 *      Author: its2016
 */

#include "../Includes.h"

#ifndef SIMULATION_SIMULATION_H_
#define SIMULATION_SIMULATION_H_

namespace simc {

class Simulation
{
public:
	Simulation();
	virtual ~Simulation();

	virtual void initializeSimulation() = 0;
	virtual void setTraceName(uint16_t connectionCode) = 0;
	virtual void defineTraceOption(std::string option) = 0;
	virtual std::vector<std::string> getTraceOptions() = 0;
	virtual void setSimulationParameter(std::string param, int position) = 0;
	virtual void closeTrace() = 0;

	void processTraceOptions();

	std::string traceName;
	sc_trace_file *fp = NULL;
};

} /* namespace simc */

#endif /* SIMULATION_SIMULATION_H_ */

/*
 * SmartSensorTop.cpp
 *
 *  Created on: 21 Mar 2018
 *      Author: its2016
 */

#include "SmartSensorTop.h"

namespace simc {

SmartSensorTop::SmartSensorTop()
{
	pSensor = new Sensor("Sensor");
	clock = new sc_core::sc_clock("clk", 1, sc_core::SC_NS);
}

SmartSensorTop::~SmartSensorTop()
{
	delete this->pSensor;
	delete this->clock;
}

void SmartSensorTop::setSimulationParameter(std::string param, int position)
{
	unsigned long val;

	if (position == 0)
	{
		//sc_time tempTime = (sc_time)strtoul(param.c_str(), NULL, 10);
		this->pSensor->Adv->nextStop = sc_time_stamp() + sc_time(stepSize, stepUnit);
	}
	else if (position == 4)
	{
		val = strtoul(param.c_str(), NULL, 10);
		this->pSensor->Gen->customData.nb_write(val);
	}
	else if (position == 6)
	{
		val = strtoul(param.c_str(), NULL, 10);
		this->pSensor->Ant->receivingEnergy = (double)val;
	}
}

void SmartSensorTop::initializeSimulation()
{
	pSensor->clkIn(*clock);

	sc_report_handler::set_actions (SC_WARNING, SC_DO_NOTHING);		// disable warnings
	pthread_mutex_init(&bufUse, NULL);

	pthread_mutex_lock(&bufUse);									// lock the pthread (?)
	appendBufferObject(&buffer, bufnames[0]);						// Append buffer object (to the list, define the names)
	appendBufferObject(&buffer, bufnames[1]);
	appendBufferObject(&buffer, bufnames[2]);
	appendBufferObject(&buffer, bufnames[3]);
	appendBufferObject(&buffer, bufnames[4]);
	appendBufferObject(&buffer, bufnames[5]);
	appendBufferObject(&buffer, bufnames[6]);
	pthread_mutex_unlock(&bufUse);
}

void SmartSensorTop::setTraceName(uint16_t connectionCode)
{
	traceName = "smart_sensor_" + Str(connectionCode);
	this->fp = sc_core::sc_create_vcd_trace_file(traceName.c_str());
	this->fp->set_time_unit(100,SC_PS);
}

void SmartSensorTop::defineTraceOption(std::string option)
{
	if (option.compare("tAdvance") == 0)
	{
		sc_trace(this->fp,  pSensor->Adv->tAdvance, "tAdvance");
	}
	else if (option.compare("advanceTime") == 0)
	{
		sc_trace(this->fp, pSensor->Adv->advanceTime, "advanceTime");
	}
	else if (option.compare("EnergyLoad") == 0)
	{
		sc_trace(this->fp, pSensor->Batt->load, "EnergyLoad");
	}
	else if (option.compare("Battenabled") == 0)
	{
		sc_trace(this->fp, pSensor->Batt->enabled, "Battenabled");
	}
	else if (option.compare("EnergyIntake") == 0)
	{
		sc_trace(this->fp, pSensor->Ant->receivingEnergy, "EnergyIntake");
	}
	else if (option.compare("Charge") == 0)
	{
		sc_trace(this->fp, pSensor->Batt->charge, "Charge");
	}
	else if (option.compare("Voltage") == 0)
	{
		sc_trace(this->fp, pSensor->Batt->voltage, "Voltage");
	}
	else if (option.compare("recData") == 0)
	{
		sc_trace(this->fp, pSensor->Ant->data, "recData");
	}
	else if (option.compare("MsgInt") == 0)
	{
		sc_trace(this->fp, pSensor->Recv->ready, "MsgInt");
	}
	else if (option.compare("DataRcvToUC") == 0)
	{
		sc_trace(this->fp, pSensor->Recv->dataOut, "DataRcvToUC");
	}
	else if (option.compare("DataSending") == 0)
	{
		sc_trace(this->fp, pSensor->Recv->sendData, "DataSending");
	}
	else if (option.compare("UCState") == 0)
	{
		sc_trace(this->fp,  pSensor->uc->state, "UCState");
	}
	else if (option.compare("UCSubstate1") == 0)
	{
		sc_trace(this->fp, pSensor->uc->substate1, "UCSubstate1");
	}
	else if (option.compare("MessageClass") == 0)
	{
		sc_trace(this->fp, pSensor->uc->messageClass, "MessageClass");
	}
	else if (option.compare("clock") == 0)
	{
		sc_trace(this->fp, pSensor->clkIn, "clock");
	}
	else if (option.compare("AntennaClock") == 0)
	{
		sc_trace(this->fp, pSensor->trmClk, "AntennaClock");
	}
	else if (option.compare("SystemClock") == 0)
	{
		sc_trace(this->fp, pSensor->sysClk, "SystemClock");
	}
	else if (option.compare("DataUCToTrm") == 0)
	{
		sc_trace(this->fp,  pSensor->UCOut, "DataUCToTrm");
	}
	else if (option.compare("msgSend") == 0)
	{
		sc_trace(this->fp, pSensor->transmitData, "msgSend");
	}
	else if (option.compare("DataMemToUC") == 0)
	{
		sc_trace(this->fp, pSensor->dataFromMemory, "DataMemToUC");
	}
	else if (option.compare("DataUCToMem") == 0)
	{
		sc_trace(this->fp, pSensor->dataToMemory, "DataUCToMem");
	}
	else if (option.compare("MemAddr") == 0)
	{
		sc_trace(this->fp, pSensor->memoryAddress, "MemAddr");
	}
	else if (option.compare("MemWrite") == 0)
	{
		sc_trace(this->fp, pSensor->writeToMemory, "MemWrite");
	}
}

std::vector<std::string> SmartSensorTop::getTraceOptions()
{
	std::vector<std::string> traceOptions;

	traceOptions.push_back("tAdvance");
	traceOptions.push_back("advanceTime");
	traceOptions.push_back("EnergyLoad");
	traceOptions.push_back("Battenabled");
	traceOptions.push_back("EnergyIntake");
	traceOptions.push_back("Charge");
	traceOptions.push_back("Voltage");
	traceOptions.push_back("recData");
	traceOptions.push_back("MsgInt");
	traceOptions.push_back("DataRcvToUC");
	traceOptions.push_back("DataSending");
	traceOptions.push_back("UCState");
	traceOptions.push_back("UCSubstate1");
	traceOptions.push_back("clock");
	traceOptions.push_back("AntennaClock");
	traceOptions.push_back("SystemClock");
	traceOptions.push_back("DataUCToTrm");
	traceOptions.push_back("msgSend");
	traceOptions.push_back("DataMemToUC");
	traceOptions.push_back("DataUCToMem");
	traceOptions.push_back("MemAddr");
	traceOptions.push_back("MemWrite");

	return traceOptions;
}

Sensor* SmartSensorTop::getSensor()
{
	return this->pSensor;
}

void SmartSensorTop::closeTrace()
{
	sc_close_vcd_trace_file(this->fp);

	std::string command = "python ./traceExpander2.py " + traceName + ".vcd";
	system(command.c_str());

	pthread_mutex_lock(&bufUse);
	deleteBuffer(&buffer);
	pthread_mutex_unlock(&bufUse);
}


} /* namespace simc */

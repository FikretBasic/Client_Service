/*
 * CounterTop.h
 *
 *  Created on: 21 Mar 2018
 *      Author: its2016
 */

#include "Simulation.h"
#include "counter/Counter.h"

#ifndef SIMULATION_COUNTERTOP_H_
#define SIMULATION_COUNTERTOP_H_

namespace simc {

class CounterTop : public Simulation
{
public:
	CounterTop();
	virtual ~CounterTop();

	void initializeSimulation();
	void setTraceName(uint16_t connectionCode);
	void defineTraceOption(std::string option);
	std::vector<std::string> getTraceOptions();
	void setSimulationParameter(std::string param, int position);
	void closeTrace();

private:
	counter *module_test;
	sc_core::sc_clock *clock;
	sc_signal<bool> reset;
	sc_signal<bool> enable;
	sc_signal<sc_uint<4 > > counter_out;
};

} /* namespace simc */

#endif /* SIMULATION_COUNTERTOP_H_ */

/*
 * Simulation.cpp
 *
 *  Created on: 21 Mar 2018
 *      Author: its2016
 */

#include "Simulation.h"

namespace simc {

Simulation::Simulation() : traceName("") {}

Simulation::~Simulation() {}

void Simulation::processTraceOptions()
{
	std::string input;

	do {
		std::cout << "Choose which variables to trace (type \"help\" for input assistance): ";
		input = getInput();

		if(input.compare("help") == 0)
		{
			std::cout << "Type \"list -tr\" for the full list of traces. \nExample writing " <<
					"the trace files (use \':\' to distinguish traces): clock:counter_out:input ..." << std::endl;
		}
		else if(input.compare("list -tr") == 0)
		{
			std::cout << "The trace options are: " << std::endl;
			for(size_t i = 0; i < this->getTraceOptions().size(); i++)
			{
				std::cout << i+1 << ". " << this->getTraceOptions().at(i) << std::endl;
			}
		}
		else
		{
			std::vector<std::string> traceTokens = split(input, ':');

			for (size_t i = 0; i < traceTokens.size(); i++)
			{
				this->defineTraceOption(traceTokens.at(i));
			}

			break;
		}
	} while(1);
}

void Simulation::closeTrace()
{
	sc_close_vcd_trace_file(this->fp);

	std::string command = "python ./traceExpander2.py " + traceName + ".vcd";
	system(command.c_str());

	pthread_mutex_lock(&bufUse);
	deleteBuffer(&buffer);
	pthread_mutex_unlock(&bufUse);
}

} /* namespace simc */

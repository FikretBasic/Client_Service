/*
 * CounterTop.cpp
 *
 *  Created on: 21 Mar 2018
 *      Author: its2016
 */

#include "CounterTop.h"

namespace simc {

CounterTop::CounterTop()
{
	module_test = new counter("Module_Test");
	clock = new sc_core::sc_clock("clk", 1, sc_core::SC_NS);
}

CounterTop::~CounterTop()
{
	delete this->module_test;
	delete this->clock;
}

void CounterTop::setSimulationParameter(std::string param, int position) {}

void CounterTop::initializeSimulation()
{
	module_test->clock(*clock);
	module_test->enable(enable);
	module_test->reset(reset);
	module_test->counter_out(counter_out);

	enable.write(1);
}

void CounterTop::setTraceName(uint16_t connectionCode)
{
	traceName = "counter_" + Str(connectionCode);
	this->fp = sc_core::sc_create_vcd_trace_file(traceName.c_str());
	this->fp->set_time_unit(100,SC_PS);
}

void CounterTop::defineTraceOption(std::string option)
{
	if (option.compare("clock") == 0)
	{
		sc_core::sc_trace(this->fp, *this->clock, "clk_in");
	}
	else if (option.compare("counter_out") == 0)
	{
		sc_trace(this->fp, counter_out, "counter");
	}
}

std::vector<std::string> CounterTop::getTraceOptions()
{
	std::vector<std::string> traceOptions;
	traceOptions.push_back("clock");
	traceOptions.push_back("counter_out");

	return traceOptions;
}

void CounterTop::closeTrace()
{
	sc_close_vcd_trace_file(this->fp);
}

} /* namespace simc */

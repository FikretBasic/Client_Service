/*
 * Receiver.h (Transmissiondata and Energy gets inserted here)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef RECEIVER_H_
#define RECEIVER_H_

#include "includes.h"
#include "utils.h"
#include "buffer.h"

SC_MODULE(Receiver)
{
public:
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk;
	sc_in<BYTE> dataIn;
	sc_in<bool> dataAvailable;
	sc_in<bool> sendData;
	sc_in<bool> inputFinished;
	sc_out<BYTE> dataOut;
	sc_out<bool> ready;
	sc_out<double> Rload;


	SC_CTOR(Receiver)
	{
		load = 0;
		locSend = 0;
		msgLen = 0;

		for(size_t iter = 0; iter < 516; iter++)
			memory[iter] = 0;

		for(size_t iter = 0; iter < 258; iter++)
		{
			data[iter] = 0;
			par[iter] = 0;
		}

		memoryFree = true;
		dataReceived = false;
		dataReady = false;

		SC_THREAD(newData);
		sensitive << dataAvailable.pos();

		SC_THREAD(controlData);
		sensitive << inputFinished.pos();

		SC_THREAD(transmitData);
		sensitive << clk.pos();

	}

	void newData()
	{
		while(1)
		{
			wait();
			load += 1;
			if(memoryFree)
			{
				dataReceived = true;
				memory[msgLen++] = dataIn.read();
			}
		}
	}

	void controlData()
	{
		while(1)
		{
			wait();
			if(dataReceived == false)
				continue;
			load += 3;
			memoryFree = false;
			bool ok = true;
			int dl = 0;
			for(int i = 0; i < msgLen;)
			{
				data[dl] = memory[i];
				par[dl] = memory[i+1];
				memory[i] = 0;
				memory[i+1] = 0;
				i += 2;
				dl++;
			}
			msgLen = dl;

			for(int i = 0; i < dl; i++)
			{
				if(par[i] != CalcParity(data[i], true))
				{
					ok = false;
				}
			}
			char f = 0, s = 0;
			if(ComputeCRC(CRC_A,(char*)data,dl-2,&f,&s) == false)
				ok = false;
			if(data[dl-2] != (unsigned char)f|| data[dl-1] != (unsigned char)s)
				ok = false;
			if(ok)
			{
				msgLen -= 2;
				locSend = 0;
				dataReady = true;
				clearBufferValuesOf(&adv, advnames[0]);
				appendBufferValue(&adv, advnames[0], "1");
			}
			else
			{
				msgLen = 0;
				memoryFree = true;
			}
		}
	}

	void transmitData()
	{
		while(true)
		{
			wait();
			load += 1;
			if(sendData.read() == true && memoryFree == false)
			{
				load += 2;
				dataOut.write(data[locSend]);
				data[locSend] = 0;
				if(locSend++ == msgLen)
				{
					dataReady = false;
					memoryFree = true;
					dataReceived = false;
					msgLen = 0;
					locSend = 0;
					dataOut.write(0);
				}
			}
			ready.write(dataReady);
			Rload.write(load);
			load = 0;
		}
	}

private:
	double load;
	BYTE locSend;
	BYTE msgLen;
	BYTE memory[516];
	BYTE data[258];
	BYTE par[258];
	bool memoryFree;
	bool dataReceived;
	bool dataReady;

};



#endif /* RECEIVER_H_ */

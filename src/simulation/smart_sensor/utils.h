/*
 * utils.h
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "includes.h"

#ifndef SIMULATION_SMART_SENSOR_UTILS_H_
#define SIMULATION_SMART_SENSOR_UTILS_H_

#define CRC_A 1
#define CRC_B 2

#define test FILENAME_MAX

unsigned char* hexToChar(std::string in, unsigned int* len = NULL);

std::string byteToHex(char* in, unsigned int l);

unsigned char CalcParity(char ch, bool odd = true);

short UpdateCRC(unsigned char ch, unsigned short* lpwCrc);

bool ComputeCRC(int type, char* data, unsigned int length, char* first, char* second);

bool ComputeSendingData(int type, char** data, unsigned int* length, char** parity);

bool CheckCRC(int type, std::string data, char** msg, unsigned int* mlen);

std::string GetCurrentWorkingDir( void );

#endif /* SIMULATION_SMART_SENSOR_UTILS_H_ */

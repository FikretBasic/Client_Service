/*
 * includes.h
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#if defined (_WIN32)
	#pragma warning(disable:4996)
	#pragma warning(disable:4005)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <systemc.h>
//#include <chrono>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <math.h>
#include <exception>
#include <pthread.h>

#include "buffer.h"

using namespace gazebo;

#ifndef INCLUDES_S_H_
#define INCLUDES_S_H_

/****************************************/
/*          global variables            */
/****************************************/
//SysC stuff

typedef unsigned char BYTE;
//const double TRANSMISSION_CLK_PERIOD = 1.0;
extern const unsigned int T_16384kHz1;
extern const unsigned int T_16MHz;
extern const unsigned int T_8192kHz;
extern const unsigned int T_8MHz;
extern const unsigned int T_4096kHz;
extern const unsigned int T_4MHz;
extern const unsigned int T_2048kHz;
extern const unsigned int T_2MHz;
extern const unsigned int T_1024kHz;
extern const unsigned int T_1MHz;
extern const unsigned int T_512kHz;
extern const unsigned int T_256kHz;
extern const unsigned int T_128kHz;
extern const unsigned int T_64kHz;
extern const unsigned int T_50kHz;
extern const unsigned int T_32kHz;
extern const unsigned int T_16kHz;
extern const unsigned int T_8kHz;
extern const unsigned int T_4kHz;
extern unsigned int SYS_CLK_PERIOD;
extern const sc_time_unit TIME_BASE;
extern const unsigned long long ticksInH;

extern int stepSize;
extern sc_time_unit stepUnit;

//sensor specs
extern const size_t MEM_SIZE;
extern const size_t NVRAM_SIZE;
extern const double MAX_BATTERY_VOLTAGE;
extern const double BATTERY_CAPACITY; //Farad
extern const double BATTERY_RESISTANCE; //Ohm
extern const double MAX_BATTERY_CHARGE; // Q Culomb
extern const double INIT_BATTERY_VOLTAGE;
extern const double INIT_BATTERY_CHARGE;
//const double MAX_BATTERY_CHARGE = 138000000 * ticksInH; // 0,138mAh = 0,1F * 5V //[pAh] -> [pATicks]
//const double INIT_BATTERY_CHARGE = 100000;//50000000 * ticksInH;
extern const unsigned int Byterate848kBit;
extern const unsigned int Byterate424kBit;//18868 / 8;
extern const unsigned int Byterate212kBit;

extern const double ANTENNA_USE;

//const double ENERGY_GAIN = 1.2057;
//message buffer
extern std::string bufnames[7];
extern std::string bufTokens[7];
extern pthread_mutex_t bufUse;
extern object* buffer;
//trace file
extern std::string filename;

//advance control
extern object* adv;
extern std::string advnames[3];


/**************************************/
/*   helper functions for debugging   */
/**************************************/

void printDebug(BYTE in);

void printDebug(std::string in);

void sendToCallingSimulation(std::string in);


/*************************************/
/*          include modules          */
/*************************************/

#include "Timer.h"
#include "Antenna.h"
#include "Battery.h"
#include "Memory.h"
#include "Sender.h"
#include "Receiver.h"
#include "Generator.h"
#include "UC.h"
#include "Advance.h"
#include "TransmissionQuartz.h"
#include "SystemQuartz.h"
#include "Sensor.h"

#endif /* INCLUDES_S_H_ */

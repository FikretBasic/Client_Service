/*
 * SystemQuartz.h (Basic Timer for CPU and Generator)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef SYSTEMQUARTZ_H_
#define SYSTEMQUARTZ_H_

#include "includes.h"

SC_MODULE(SystemQuartz)
{
public:
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clkin;
	sc_in<bool> enabled;
	sc_out<bool> clkout;

	SC_CTOR(SystemQuartz)
	{
		clock = false;
		ticks = 0;

		SC_THREAD(work);
		sensitive << clkin.pos();
	}

	void work()
	{
		while(true)
		{
			wait();
			if(enabled.read() == true && ticks++ >= SYS_CLK_PERIOD / 2)
			{
				clkout.write(clock);
				clock = !clock;
				ticks = 0;
			}

		}
	}

private:
	bool clock;
	unsigned int ticks;

};



#endif /* SYSTEMQUARTZ_H_ */

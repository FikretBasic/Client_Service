/*
 * Memory.h (non-volatile memory)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef MEMORY_H_
#define MEMORY_H_

#include "includes.h"

SC_MODULE(Memory)
{
public:
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk;
	sc_in<bool> write;
	sc_in<unsigned int> addr;
	sc_in<BYTE> dataIn;
	sc_out<double> energy;
	sc_out<BYTE> dataOut;

	SC_CTOR(Memory)
	{
		energyUsage = 0;

		SC_THREAD(work);
		sensitive << clk.pos();

		initmem();
	}

	void work()
	{
		while(1)
		{
			wait();
			energy.write(energyUsage);
			energyUsage = 100;
			unsigned int ad = addr.read();
			if(ad > 0 && ad < NVRAM_SIZE)
			{
				if(write.read() == true)
				{
					energyUsage += 2;
					mem[ad] = dataIn.read();
				}
				else
				{
					energyUsage += 1;
					dataOut.write(mem[ad]);
				}
			}
			if(ad == 0)
			{
				dataOut.write(0);
			}
		}
	}

private:
	double energyUsage;
	// NVRAM_SIZE
	BYTE mem[4096];

	void initmem()
	{
		//CapabilityContainer
		BYTE cc[0x1F] = {0,0x17,2,0,0x3b,0,0x3b,4,6,0xe1,4,0,0x20,0,0xff,4,6,0xe1,5,0,0xff,0,0,4,6,0xe1,5,0,4,0,0xff};
		//std::string cc = "\x00\x17\x02\x00\x3B\x00\x3B\x04\x06\xE1\x04\x00\x20\x00\xFF\x04\x06\xE1\x05\x00\xFF\x00\x00";
		//                  length  ver rapdu l capdu l TLV1    e104    size    rAccwAccTLV2    e105    size    rAccwAcc
		//http://apps4android.org/nfc-specifications/NFCForum-TS-Type-4-Tag_2.0.pdf pp10
		int e103 = 2*1024;
		for(unsigned int i = 0; i < 0x1F; i++)
		{
			mem[e103 + i] = cc[i];
		}
		std::string se104 = "SensorTestFile\nDo not read me :)";
		BYTE e104ndef[5] = {0,0x23,0xd4,0x20,0x00};
		int e104 = 3*1024;
		for(unsigned int i = 0; i < 5; i++)
		{
			mem[e104 + i] = e104ndef[i];
		}
		for(unsigned int i = 0; i < se104.length(); i++)
		{
			mem[e104 + i + 5] = se104[i];
		}

		int e105 = 3*1024 + 0x100;
		for(unsigned int i = 0; i  < 0xff; i++)
		{
			mem[e105 + i] = 0;
		}

	}
};



#endif /* MEMORY_H_ */

/*
 * Battery.h (Stores energy)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef BATTERY_H_
#define BATTERY_H_

#include "includes.h"

SC_MODULE(Battery)
{
public:
	double lastload;
	//double voltage = 0;
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<double> load; // Ampere
	sc_in<bool> clk;
	sc_out<bool> enabled;
	sc_out<double> voltage;
	//sc_out<bool> alert; // unused

	double charge;

	SC_CTOR(Battery)
	{
		internalVoltage = 0;
		lastload = 0;
		charge = INIT_BATTERY_CHARGE;

		SC_THREAD(work);
		sensitive << clk.pos();
	};

	void work()
	{
		enabled.write(true);
		while(true)
		{
			wait();
			double l = load.read();
			// nA -> A (Q/s)
			l = l / 1000000000;
			if(l < 0) // charging
			{
				internalVoltage = 1.01 * MAX_BATTERY_VOLTAGE;
				double maxL = ((1.01 * MAX_BATTERY_VOLTAGE) - (charge / BATTERY_CAPACITY)) / BATTERY_RESISTANCE; // A
				if(l < -maxL)
					l = -maxL;
			}
			else
			{
				internalVoltage = (charge / BATTERY_CAPACITY) - (l * BATTERY_RESISTANCE);
			}

			voltage.write(internalVoltage);
			// Q/s -> Q/ns
			l = l/1000000000;

			//std::cout << "Hallo, are you here: " << l << std::endl;

			charge -= l;
			lastload = l;
			if(charge < 0)
				charge = 0;
			if(charge > MAX_BATTERY_CHARGE)
				charge = MAX_BATTERY_CHARGE;
			if(internalVoltage <= 1.5)
				enabled.write(false);
			if(internalVoltage >= 2)
				enabled.write(true);
		}
	}

private:
	double internalVoltage;

};




#endif /* BATTERY_H_ */

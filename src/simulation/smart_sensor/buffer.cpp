/*
 * buffer.cpp
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "buffer.h"

namespace gazebo{

bool modifyObjectDescription(object** inbuffer, std::string name, std::string desc)
{
	if(inbuffer == NULL)
		return false;
	if(name.empty())
		return false;
	if(desc.empty())
		return false;
	object* t = *inbuffer;
	if(t == NULL)
		return false;
	while(t != NULL)
	{
		if(t->name == name)
		{
			t->description = desc;
			return true;
		}
		t = t->next;
	}
	return false;
}

std::string getObjectDescription(object** inbuffer, std::string name)
{
	if(inbuffer == NULL)
		return "";
	if(name.empty())
		return "";
	object* t = *inbuffer;
	if(t == NULL)
		return "";
	while(t != NULL)
	{
		if(t->name == name)
		{
			if(t->description.empty())
				return "";
			return t->description;
		}
		t = t->next;
	}
	return "";
}

bool appendBufferObject(object** inbuffer, std::string name)
{
	if(inbuffer == NULL)
		return false;
	if(name.empty())
		return false;
	object* t = *inbuffer;
	if (t == NULL) // init buffer
	{
		t = new object;
		t->name = name;
		t->data = NULL;
		t->next = NULL;
		*inbuffer = t;
	}
	else
	{
		while(t->next != NULL)
		{
			if(t->name == name) // name already exists
			{
				//std::cout << "Buffer error: Object: \"" << name <<"\" already exists\n";
				return true;
			}
			t = t->next;
		}
		t->next = new object;
		t = t->next;
		t->name = name;
		t->data = NULL;
		t->next = NULL;
	}
	return true;
}

bool appendBufferValue(object** inbuffer, std::string name, std::string ivalue)
{
	if(inbuffer == NULL)
		return false;
	if(name.empty())
		return false;
	if(ivalue.empty())
		return false;
	object* t = *inbuffer;
	if(t == NULL) // init buffer
	{
		std::cout << "init\n";
		t = new object;
		t->name = name;
		t->data = new value;
		t->data->value = ivalue;
		t->data->next = NULL;
		t->next = NULL;
		*inbuffer = t;
	}
	else
	{
		while(t->next != NULL && t->name != name) // go until element is found or to end of list
		{
			t = t->next;
		}
		if(t->name != name) // object not found
		{
			//std::cout << "append object before value\n";
			appendBufferObject(inbuffer, name);
			return appendBufferValue(inbuffer, name,ivalue);
		}
		else
		{
			//std::cout << "appending something\n";
			if(t->data == NULL)
			{
				//std::cout << "first value :" <<ivalue<<" in "<<t->name<<" list\n";
				t->data = new value;
				t->data->value = ivalue;
				t->data->next = NULL;
			}
			else
			{
				value* v = t->data;
				while(v->next != NULL) // go until end of value list
				{
					//std::cout << v->value << " ";
					v = v->next;
				}
				//std::cout << " append " <<  ivalue << " to " << t->name << std::endl;
				v->next = new value;
				v = v->next;
				v->value = ivalue;
				v->next = NULL;
			}
			return true;
		}
	}
	return false;
}

std::string getBufferValue(object** inbuffer, std::string name, int val)
{
	if(inbuffer==NULL)
		return "";
	if(name.empty())
		return "";
	object* t = *inbuffer;
	if(t == NULL)
	{
		//std::cout << "Buffer error: printting not possible - not initialized\n";
		return "";
	}
	while(t != NULL && t->name != name)
	{
		t = t->next;
	}
	if(t == NULL)
	{
		return "";
	}
	if(t->name != name)
	{
		//std::cout << "Object \"" << name << "\" not found\n";
		return "";
	}
	if(t->data == NULL)
	{
		return "";
	}
	else
	{
		value* v = t->data;
		int i = 0;
		while(i < val && v != NULL)
		{
			v = v->next;
			i++;
		}
		if(v != NULL)
		{
			 return v->value;
		}
	}
	return "";
}

bool modifyValue(object** inbuffer, std::string name, std::string ivalue, int val)
{
	if(inbuffer==NULL)
		return false;
	if(name.empty())
		return false;
	object* t = *inbuffer;
	if(t == NULL)
	{
		return false;
	}
	while(t != NULL && t->name != name)
	{
		t = t->next;
	}
	if(t->name != name)
	{
		//std::cout << "Object \"" << name << "\" not found\n";
		return false;
	}
	if(t->data == NULL)
	{
		return false;
	}
	else
	{
		value* v = t->data;
		int i = 0;
		while(i < val && v != NULL)
		{
			v = v->next;
			i++;
		}
		if(v != NULL)
		{
			v->value = ivalue;
			return true;
		}
	}
	return false;
}

std::string getBufferOValues(object** inbuffer, std::string name)
{
	std::string ret = "";
	if(inbuffer == NULL)
		return "";
	if(name.empty())
		return "";
	object* t = *inbuffer;
	if (t == NULL)
	{
		return "";
	}
	while(t != NULL && t->name != name)
	{
		t = t->next;
	}
	if(t->name != name)
	{
		return "";
	}
	if(t->data == NULL)
	{
		return "";
	}
	else
	{
		value* v = t->data;
		while(v != NULL)
		{
			ret += v->value + " ";
			v = v->next;
		}
		return ret.substr(0,ret.length() - 1);
	}
	return "";
}

int getNumValuesOf(object** inbuffer, std::string name)
{
	if(inbuffer == NULL)
		return 0;
	if(name.empty())
		return 0;
	object* t  = *inbuffer;
	if(t == NULL)
		return 0;
	while(t != NULL)
	{
		if(t->name == name)
		{
			int ret = 0;
			value* v = t->data;
			while(v != NULL)
			{
				v = v->next;
				ret++;
			}
			return ret;
		}
		t = t->next;
	}
	return 0;
}

int getNumValues(object** inbuffer)
{
	if(inbuffer == NULL)
		return 0;
	object* t = *inbuffer;
	if(t == NULL)
		return 0;
	int ret = 0;
	while(t != NULL)
	{
		ret += getNumValuesOf(inbuffer,t->name);
		t = t->next;
	}
	return ret;
}

bool printBufferContents(object** inbuffer)
{
	if(inbuffer == NULL)
		return false;
	object* t = *inbuffer;
	if (t == NULL)
	{
		return false;
	}
	while(t != NULL)
	{
		if(t->data != NULL)
		{
			std::cout << t->name;
			if(!t->description.empty())
				std::cout << " - " << t->description;
			std::cout << ": ";
			value* v = t->data;
			while(v != NULL)
			{
				std::cout << v->value << "; ";
				v = v->next;
			}
			std::cout << std::endl;
		}
		t = t->next;
	}
	return true;
}

bool clearBufferValues(object** inbuffer, std::string excludes[], int len)
{
	if(inbuffer==NULL)
		return false;
	object* t = *inbuffer;
	if(t == NULL)
	{
		return false;
	}
	while(t != NULL)	// go through every object
	{
		bool ok = true;
		if(excludes != NULL)
		{
			for(int i = 0; i < len; i++)
			{
				try{
					if(t->name == excludes[i])
					{
						ok = false;
					}
				}
				catch(...)
				{
					std::cout << "try to access exclude item out of bounds\n";
				}
			}
		}
		if(ok && t->data != NULL)
		{
			//std::cout << "deleting from: " << t->name << std::endl;
			value *v = t->data;
			value *vn = NULL;
			while(v != NULL)	//save the next value and delete the old one - deleted from the beginning
			{
				vn = v->next;
				delete(v);
				v = vn;
			}
			t->data = NULL;
		}
		//else
		//{
			//std::cout << "not deleting: " << t->name << " data: " << t->data << std::endl;
		//}
		t = t->next;
	}
	return true;
}

bool clearBufferValuesOf(object** inbuffer, std::string name)
{
	if(inbuffer == NULL)
		return false;
	if(name.empty())
		return false;
	object* t  = *inbuffer;
	if(t == NULL)
		return false;
	while(t != NULL)
	{
		if(t->name == name && t->data != NULL)
		{
			value* v = t->data;
			value* vn = NULL;
			while(v != NULL)
			{
				vn = v->next;
				delete(v);
				v = vn;
			}
			t->data = NULL;
		}
		t = t->next;
	}
	return true;
}

bool clearBufferValue(object** inbuffer, std::string name, int val)
{
	if(inbuffer == NULL)
		return false;
	if(name.empty())
		return false;
	object* t = *inbuffer;
	if(t == NULL)
		return false;
	while(t != NULL)
	{
		if(t->name == name)
		{
			if(t->data != NULL)
			{
				value* v = t->data;
				value* vl = NULL;
				int i = 0;
				while(i++ < val && v != NULL)
				{
					vl = v;
					v = v->next;
				}
				if(v != NULL)
				{
					if(vl != NULL)
					{
						vl->next = v->next;
					}
					else
					{
						t->data = v->next;
					}
					delete(v);
					return true;
				}
				else
					return false;
			}
			return false;

		}
		t = t->next;
	}
	return false;
}

bool deleteBufferObject(object** inbuffer, std::string name)
{
	if(inbuffer == NULL)
		return false;
	object* t = *inbuffer;
	if(t == NULL)
	{
		return true;
	}
	object* lt = NULL;
	while(t->next != NULL && t->name != name) // go until object is found or to the end
	{
		lt = t;
		t = t->next;
	}
	if(t->name != name) // object is not found
	{
		return false;
	}
	else
	{
		if(t->data != NULL) // it the object has values stored delete them first
		{
			value* v = t->data;
			value *vn = NULL;
			while(v != NULL)	//save the next value and delete the old one - deleted from the beginning
			{
				vn = v->next;
				delete(v);
				v = vn;
			}
			t->data = NULL;
		}
		if(lt != NULL)
		{
			lt->next = t->next;
		}
		else
		{
			*inbuffer = t->next;
		}
		delete(t);
	}
	return true;
}

std::string getAndRemoveValue(object** inbuffer, std::string name, int value)
{
	std::string ret = getBufferValue(inbuffer, name, value);
	clearBufferValue(inbuffer, name, value);
	return ret;
}

bool deleteBuffer(object** inbuffer)
{
	if(inbuffer == NULL)
		return false;
	if(*inbuffer==NULL)
	{
		return true;
	}
	while(*inbuffer != NULL)
	{
		std::string name = (*inbuffer)->name;
		deleteBufferObject(inbuffer,name);
	}
	return true;
}

bool searchTokenAndAppend(object** inbuffer, std::string token, std::string name, std::string in)
{
	//std::cout << "searching " << token << " in " << in << std::endl;
	if(inbuffer == NULL)
		return false;
	if(token.empty())
		token = name;
	if(name.empty())
		return false;
	if(in.empty())
		return false;
	object* t = *inbuffer;
	if(t == NULL)
	{
		appendBufferObject(inbuffer,name);
		return searchTokenAndAppend(inbuffer,token,name,in);
	}
	while(t != NULL)
	{
		if(t->name == name)
		{
			size_t begin,end;
			begin = end = 0;
			bool ret = false;
			while(begin != std::string::npos)
			{
				std::string f = "<" + token + ">";
				std::string b = "<\\" + token + ">";
				begin = in.find(f,end);
				end = in.find(b,begin);
				if(end != std::string::npos)
				{
					ret = appendBufferValue(inbuffer,name,in.substr(begin + f.length(), end - (begin + f.length())));
				}
			}
			return ret;
		}
		t = t->next;
	}
	if(t == NULL)
	{
		appendBufferObject(inbuffer,name);
		return searchTokenAndAppend(inbuffer,token,name,in);
	}
	return false;
}

bool searchAndAppendAllTokens(object** inbuffer, std::string in)
{
	if(inbuffer == NULL)
		return false;
	if(in.empty())
		return false;
	object* t = *inbuffer;
	if(t == NULL)
		return false;
	while(t != NULL)
	{
		std::string token = t->name;
		//std::cout << "searching for " << token << std::endl;
		searchTokenAndAppend(inbuffer,token,token,in);
		t = t->next;
	}
	return true;
}

}

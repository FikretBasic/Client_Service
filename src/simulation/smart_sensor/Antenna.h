/*
 * Antenna.h (belongs to channel - data to be received is stored here and given to the receiver)
 *
 *  Created on: May 19, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef ANTENNA_H_
#define ANTENNA_H_

#include "includes.h"
#include "buffer.h"

SC_MODULE(Antenna)
{
public:
	std::string receiving;
	double receivingEnergy;
	/*****************/
	/*    Modules    */
	/*****************/

	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk; // from TransmissionQuartz
	sc_in<bool> eclk; // globalClk to update Energy intake
	sc_out<BYTE> data;
	sc_out<bool> save;
	sc_out<double> energy;
	sc_out<double> load;
	sc_out<bool> dataFinished;


	SC_CTOR(Antenna)
	{
		receiving = "";
		receivingEnergy = 0;

		rce = 0;
		bitReceived = 0;

		SC_THREAD(work);
		sensitive << clk.pos();

		SC_THREAD(getEnergy);
		sensitive << eclk.pos();

		SC_THREAD(checkInputs);
		sensitive << eclk.pos();
	}

	void checkInputs()
	{
		while(true)
		{
			wait();
			if(receiving.empty())
			{
				pthread_mutex_lock(&bufUse);
				int n = getNumValuesOf(&buffer, bufnames[3]);
				pthread_mutex_unlock(&bufUse);
				if(n > 0)
				{
					pthread_mutex_lock(&bufUse);
					std::string cmd = getBufferValue(&buffer, bufnames[3]);
					clearBufferValue(&buffer, bufnames[3]);
					pthread_mutex_unlock(&bufUse);
					receiving = cmd;

					clearBufferValuesOf(&adv, advnames[2]);
					appendBufferValue(&adv, advnames[2], "1");
				}
			}
		}
	}

	void getEnergy()
	{
		energy.write(0);
		while(true)
		{
			wait();
			// converting mA to nA
			energy.write(receivingEnergy * 1000000);
		}
	}

	void work()
	{
		load.write(ANTENNA_USE);
		dataFinished.write(true);
		save.write(false);
		data.write(0);
		while(true)
		{
			wait();
			if(!receiving.empty())
			{
				bitReceived ++;
				if(bitReceived >= 8)
				{
					dataFinished.write(false);
					BYTE d = strtoul(receiving.substr(0,2).c_str(), NULL, 16);
					data.write(d);
					save.write(true);
					receiving = receiving.substr(2);
					wait(SC_ZERO_TIME);
					save.write(false);
					if(bitReceived == 9)
						bitReceived = 0;
				}
			}
			else
			{
				save.write(false);
				data.write(0);
				dataFinished.write(true);
				pthread_mutex_lock(&bufUse);
				std::string cmd = getBufferValue(&buffer, bufnames[3]);
				clearBufferValue(&buffer, bufnames[3]);
				pthread_mutex_unlock(&bufUse);
				receiving = cmd;
				//rce = false;
			}
			if(receiving.empty())
			{
				if(rce++ > 3)
				{
					clearBufferValuesOf(&adv, advnames[2]);
					appendBufferValue(&adv, advnames[2], "0");
				}
			}
			else
			{
				rce = 0;
				clearBufferValuesOf(&adv, advnames[2]);
				appendBufferValue(&adv, advnames[2], "1");
			}
		}
	}

private:
	int rce;
	int bitReceived;

};



#endif /* ANTENNA_H_ */

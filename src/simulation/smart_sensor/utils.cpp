/*
 * utils.cpp
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "utils.h"

/*
 * utils.cpp
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */


unsigned char* hexToChar(std::string in, unsigned int* len)
{
	if(in.length() % 2 == 1)
		return NULL;
	if(len != NULL)
		*len = in.length() / 2;
	unsigned char* out = (unsigned char*) malloc(in.length() / 2);
	int j = 0;
	for(unsigned int i = 0; i < in.length(); i += 2)
	{
		unsigned char c;
		const char ic[3] = {in[i], in[i+1],0};
		c = strtoul(ic,NULL, 16);
		out[j] = c;
		j++;
	}
	return out;
}

std::string byteToHex(char* in, unsigned int l)
{
	std::string o = "";
	char* buf = (char*) malloc(3 * sizeof(char));
	for(unsigned int i = 0; i < l; i++)
	{
		sprintf(buf, "%02X", (unsigned char)(in[i]));
		o += buf;
	}
	free(buf);
	return o;
}

unsigned char CalcParity(char ch, bool odd)
{
	char p = 0;
	if(odd)
		p = 1;
	for(int i = 0; i < 8; i++)
	{
		if((ch & (0x01 << i)) != 0)
		{
			p = 1 - p;
		}
	}
	return p;
}

short UpdateCRC(unsigned char ch, unsigned short* lpwCrc)
{
	ch = (ch ^ (unsigned char)(*lpwCrc & 0x00FF));
	ch = (ch ^ (ch << 4));
	*lpwCrc = ((*lpwCrc >> 8) ^ ((unsigned short)ch << 8) ^ ((unsigned short)ch << 3) ^ ((unsigned short)ch >> 4));
	return *lpwCrc;
}

bool ComputeCRC(int type, char* data, unsigned int length, char* first, char* second)
{
	if(data == NULL || first == NULL || second == NULL)
		return false;
	unsigned short wCrc;
	if(type == CRC_A)
		wCrc = 0x6363;
	else if(type == CRC_B)
		wCrc = 0xFFFF;
	else
		return false;
	unsigned char chBlock;
	int i = 0;
	do {
		chBlock = data[i++];
		UpdateCRC(chBlock, &wCrc);
	}while(--length);
	if(type == CRC_B)
	{
		wCrc = ~wCrc;
	}
	*first = wCrc & 0xFF;
	*second = (wCrc >> 8) & 0xFF;
	return true;
}

bool ComputeSendingData(int type, char** data, unsigned int* length, char** parity)
{
	if(data == NULL || length == NULL || parity == NULL)
		return false;

	char f,s;
	if(ComputeCRC(type, *data, *length, &f, &s) == false)
		return false;

	*length += 2;
	char* ndata = (char*) realloc(*data, (*length) * sizeof(char));
	if(ndata == NULL)
		return false;
	*data = ndata;
	//char* ndata = (char*) malloc((*length) * sizeof(char));
	//memcpy(ndata,*data,(*length) - 2);
	(*data)[*length - 2] = f;
	(*data)[*length - 1] = s;
	//free(*data);
	//*data = ndata;

	//*data = (char*) realloc(*data, *length * sizeof(char));
	if(*parity != NULL)
		free(*parity);
	*parity = (char*) malloc((*length) * sizeof(char));

	for(unsigned int i = 0; i < *length; i++)
	{
		(*parity)[i] = CalcParity((*data)[i], true);
	}
	return true;
}

bool CheckCRC(int type, std::string data, char** msg, unsigned int* mlen)
{
	unsigned int len = 0;
	//std::cout << "crc0: " << data << std::endl;
	unsigned char* idat = hexToChar(data, &len);
	if(idat == NULL || len == 0)
		return false;
//	for(unsigned int i = 0; i < len; i++)
//		printf("crc0a: %X ",idat[i]);
	char* dat = (char*) malloc((len/2) * sizeof(char));
	char* par = (char*) malloc((len/2) * sizeof(char));
	int j = 0;
//	printf("\n");
	for(unsigned int i = 0; i < len; i += 2)
	{
		dat[j] = idat[i];
		par[j] = idat[i+1];
		j++;
	}
	free(idat); idat = NULL;
	len = len / 2;
//	for(unsigned int i = 0; i < len; i++)
//		printf("crc0b: %X %X ", dat[i],par[i]);
//	std::cout << "ccrc1\n";
	for(unsigned int i = 0; i < len; i++)
	{
//		printf("ccrc1aa: %X\n", dat[i]);
		if(par[i] != CalcParity(dat[i], true))
		{
			free(dat);free(par);
//			std::cout << "ccrc1b\n";
			return false;
		}
//		else
//			std::cout << "ccrc1a\n";
	}
//	std::cout << "ccrc2\n";
	free(par); par = NULL;
	char f,s;
	if(ComputeCRC(type, dat, len-2, &f, &s) == true)
	{
		if(f != dat[len-2] || s != dat[len-1])
		{
			free(dat);
//			std::cout << "ccrc2b\n";
			return false;
		}
//		else
//			std::cout << "ccrc2a\n";
	}
	else
	{
		free(dat);
		return false;
	}
//	std::cout << "ccrc3\n";
	if(msg && mlen)
	{
		*mlen = 2*(len - 2);
//		printf("%d\n",*mlen);
		if(*msg)
			free(*msg);
		(*msg) = (char*) malloc(((*mlen) + 1) * sizeof(char));
		std::fill(*msg, *msg + *mlen+1, 0);
		char* buf = (char*) malloc(3);
		buf[0] = buf[1] = buf[2] = 0;
		int j = 0;
		for(unsigned int i = 0; i < (len-2); i++)
		{
			sprintf(buf,"%02X", (unsigned char)dat[i]);
			//printf("%02X %s ", (unsigned char)dat[i], buf);
			//(*msg)[j++] = '0';
			//(*msg)[j++] = 'x';
			(*msg)[j++] = buf[0];
			(*msg)[j++] = buf[1];
			(*msg)[j+1] = '\0';
		}
		free(buf); buf = NULL;
		(*msg)[(*mlen)+1] = '\0';
	}
	//printf("\n%s\n", *msg);
	free(dat); dat = NULL;
	return true;
}

std::string GetCurrentWorkingDir( void ) {
  char buff[FILENAME_MAX];
  getcwd( buff, FILENAME_MAX );
  std::string current_working_dir(buff);
  return current_working_dir;
}



/*
 * Sender.h (Data to send gets dropped here)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef SENDER_H_
#define SENDER_H_

#include "includes.h"
#include "utils.h"
#include "buffer.h"

SC_MODULE(Sender)
{
public:
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk;
	sc_in<BYTE> data;
	sc_in<bool> send;
	sc_in<bool> Sclk;
	sc_out<double> load;

	sc_signal<bool> sendingData;

	SC_CTOR(Sender)
	{
		Rload = 0;

		for(size_t iter = 0; iter < 516; iter++)
			msg[iter] = 0;

		inlen = 0;
		transmit = 0;
		bitssent = 0;

		SC_THREAD(store);
		sensitive << clk.pos();

		SC_THREAD(trm);
		sensitive << Sclk.pos();

	}

	void store()
	{
		while(true)
		{
			wait();
			load.write(Rload);
			Rload = 0;
			if(send.read() == true)
			{
				Rload += 1;
				msg[inlen++] = data.read();
				clearBufferValuesOf(&adv, advnames[1]);
				appendBufferValue(&adv, advnames[1], "1");
			}
		}
	}

	void trm()
	{
		sendingData.write(false);
		while(true)
		{
			wait();
			if(transmit == 0 && inlen == 0)
			{
				clearBufferValuesOf(&adv, advnames[1]);
				appendBufferValue(&adv, advnames[1], "0");
			}
			if(transmit < inlen)
			{
				sendingData.write(true);
				clearBufferValuesOf(&adv, advnames[1]);
				appendBufferValue(&adv, advnames[1], "1");
				Rload += 3;
				bitssent++;
				if(bitssent >= 8)
				{
					transmit++;
					if(bitssent == 9)
						bitssent = 0;
				}
			}
			else
			{
				if(transmit > 0 && send.read() == false)
				{
					char* smsg = (char*) malloc(1+inlen*sizeof(char));
					memcpy(smsg,msg,inlen);
					char* spar = NULL;
					unsigned int len = inlen;
					ComputeSendingData(CRC_A, &smsg, &len, &spar);
					char* m = (char*)malloc(2*len*sizeof(char));
					int j = 0;
					for(unsigned int i = 0; i < len; i++)
					{
						m[j++] = smsg[i];
						m[j++] = spar[i];
					}
					std::cout << "<data><M>" + byteToHex(m, 2 * len) + "<\\M><\\data>" << std::endl;
					free(m);free(smsg);free(spar);
					transmit = 0;
					inlen = 0;
					sendingData.write(false);
				}
			}
		}
	}

private:
	double Rload;
	BYTE msg[516];
	int inlen;
	int transmit;
	int bitssent;

};



#endif /* SENDER_H_ */

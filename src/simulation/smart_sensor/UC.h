/*
 * UC.h (Microcontroller)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 *
 *      energy stats from MSP430 (http://www.ti.com/lit/ds/symlink/msp430g2453.pdf)
 */

#pragma once

#ifndef UC_H_
#define UC_H_

#include "includes.h"

SC_MODULE(UC)
{
public:
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk;
	sc_in<BYTE> dataFromReceiver;
	sc_out<BYTE> dataToTransmitter;
	sc_out<double> load;
	sc_out<bool> receiveData;
	sc_out<bool> sendData;
	sc_in<bool> dataAvailable;

	sc_out<unsigned int> memoryAddress;
	sc_out<bool> memoryWrite;
	sc_out<BYTE> dataToMemory;
	sc_in<BYTE> dataFromMemory;

	sc_out<bool> loadSensorData;
	sc_in<BYTE> sensorData;

	/***************/
	/*  Variables  */
	/***************/
	int state;
	int substate1;
	int messagingSubstate;
	int memoryAccessSubstate;
	int idlestate;
	int messageClass;
	int sensorState;

	SC_CTOR(UC)
	{
		state = 0;
		substate1 = 0;
		messagingSubstate = 0;
		memoryAccessSubstate = 0;
		idlestate = 0;
		messageClass = 0;
		sensorState = 0;

		Uload = 0;
		file = 0;
		memoryCounter = 0;
		e103 = 2 * 1024;
		e104 = 3 * 1024;
		e105 = 3 * 1024 + 0x100;
		e106 = 2 * 1024 + 0x100;

		SC_THREAD(work);
		sensitive << clk.pos();
	}

	int sendOk()
	{
		int r;
		Uload += 1;
		switch(messagingSubstate)
		{
		case 0:
			sendData.write(true);
			dataToTransmitter.write(0x90);
			messagingSubstate = 1;
			r = 0;
			break;
		case 1:
			sendData.write(true);
			dataToTransmitter.write(0x00);
			messagingSubstate = 2;
			r = 0;
			break;
		case 2:
			sendData.write(false);
			dataToTransmitter.write(0x00);
			messagingSubstate = 0;
			r = 1;
			break;
		default:
			messagingSubstate = 0;
			r = -1;
			break;
		}
		return r;

	}

	int sendError()
	{
		int r;
		Uload += 1;
		switch(messagingSubstate)
		{
		case 0:
			sendData.write(true);
			dataToTransmitter.write(0xC5);
			messagingSubstate = 1;
			r = 0;
			break;
		case 1:
			sendData.write(true);
			dataToTransmitter.write(0xE9);
			messagingSubstate = 2;
			r = 0;
			break;
		case 2:
			sendData.write(false);
			dataToTransmitter.write(0x00);
			messagingSubstate = 0;
			r = 1;
			break;
		default:
			messagingSubstate = 0;
			r = -1;
			break;
		}
		return r;

	}

	int loadFile(unsigned int addr, unsigned int length, unsigned int RAMstart)
	{
		Uload += 1;
		switch(memoryAccessSubstate)
		{
		case 0:
			memoryAddress.write(addr);
			memoryAccessSubstate = 1;
			break;
		case 1:
			memoryCounter = 1;
			memoryAddress.write(addr + memoryCounter);
			memoryAccessSubstate = 2;
			break;
		case 2:
			Uload += 1;
			memory[RAMstart + (memoryCounter-1)] = dataFromMemory.read();
			memoryCounter += 1;
			if(memoryCounter >= length)
			{
				memoryAddress.write(0);
				memoryAccessSubstate = 3;
				break;
			}
			memoryAddress.write(addr + memoryCounter);
			break;
		case 3:
			memory[RAMstart + (memoryCounter-1)] = dataFromMemory.read();
			memoryAccessSubstate = 0;
			memoryCounter = 0;
			return 1;
			break;
		default:
			memoryAccessSubstate = 0;
			memoryCounter = 0;
			break;
		}
		return 0;
	}

	int sendMemory(unsigned int addr, unsigned int length)
	{
		Uload += 1;
		switch(messagingSubstate)
		{
		case 0:
			sendData.write(true);
			dataToTransmitter.write(memory[addr + memoryCounter]);
			memoryCounter += 1;
			if(memoryCounter >= length)
			{
				messagingSubstate = 1;
			}
			break;
		case 1:
			sendData.write(false);
			dataToTransmitter.write(0);
			messagingSubstate = 0;
			memoryCounter = 0;
			return 1;
		default:
			messagingSubstate = 0;
			memoryCounter = 0;
			break;
		}
		return 0;
	}

	int saveMemory(unsigned int RAMaddr, unsigned int length, unsigned int addr)
	{
		Uload += 2;
		switch(memoryAccessSubstate)
		{
		case 0:
			memoryAddress.write(addr + memoryCounter);
			memoryWrite.write(true);
			dataToMemory.write(memory[RAMaddr + memoryCounter]);
			if(memoryCounter++ > length)
			{
				memoryAccessSubstate = 1;
			}
			break;
		case 1:
			memoryAddress.write(0);
			memoryWrite.write(false);
			memoryAccessSubstate = 0;
			memoryCounter = 0;
			return 1;
			break;
		default:
			memoryWrite.write(false);
			memoryAccessSubstate = 0;
			memoryCounter = 0;
			break;
		}
		return 0;
	}

	void work()
	{
		while(true)
		{
			load.write(Uload);
			Uload = 0;
			wait();
			switch(state)
			{
			case 0: // idle
				Uload += 500;
				if(idlestate == 10)
				{
					SYS_CLK_PERIOD = T_128kHz;
				}
				if(idlestate++ > 10)
				{
					clearBufferValuesOf(&adv, advnames[0]);
					appendBufferValue(&adv, advnames[0], "0");
					idlestate--;
				}
				if(dataAvailable.read() == true)
				{
					clearBufferValuesOf(&adv, advnames[0]);
					appendBufferValue(&adv, advnames[0], "1");
					SYS_CLK_PERIOD = T_1MHz;
					idlestate = 0;
					state = 1;
					substate1 = 0;
				}
				break;
			case 1: // message ready
				Uload += 230000;
				switch(substate1)
				{
				case 0:
					for(int i = 1; i < 7; i++)
					{
						memory[i] = 0;
					}
					memory[0] = 2;
					receiveData.write(true);
					substate1++;
					break;
				case 1:
					substate1++;
					break;
				case 2:
					memory[memory[0]++] = dataFromReceiver.read();
					if(dataAvailable.read() == false)
					{
						receiveData.write(false);
						substate1++;
					}
					break;
				case 3: // classify message
					if(memory[2] == 0x00 && memory[3] == 0xA4) // select
					{
						messageClass = 1;
						state = 2;
					}
					else if(memory[2] == 0x00 && memory[3] == 0xB0) // read
					{
						messageClass = 2;
						state = 2;
					}
					else if(memory[2] == 0x00 && memory[3] == 0xD6) // update
					{
						messageClass = 3;
						state = 2;
					}
					else if(memory[2] == 0xA4 && memory[3] == 0x01) // control
					{
						messageClass = 4;
						state = 2;
					}
					else
					{
						state = 0;
					}
					substate1 = 0;
					break;
				default:
					substate1 = 0;
					break;
				}
				break;
			case 2: // work on message
				Uload += 230000;
				switch(messageClass)
				{
				case 1: // select
					switch(substate1)
					{
					case 0:
						if(memory[6] == 2)
						{
							file = (memory[7] << 8) + memory[8];
							memory[7] = memory [8] = 0;
							substate1 = 1;
						}
						break;
					case 1:
						if(sendOk() == 1)
						{
							state = 0;
							messageClass = 0;
							substate1 = 0;
							for(int i = 0; i < 9; i++)
							{
								memory[i] = 0;
							}
						}
						break;
					default:
						substate1 = 0;
						break;
					}
					break;
				case 2: // read
					switch(substate1)
					{
					case 0: // check command valid
						if(file == 0xE103 || file == 0xE104 || file == 0xE105 || file == 0xE106)
						{
							if(file == 0xE103)
								file = e103;
							else if(file == 0xE104)
								file = e104;
							else if(file == 0xE105)
								file = e105;
							else if(file == 0xE106)
								file = e106;

							substate1 = 1;
						}
						else
						{
							substate1 = 5;
						}
						break;
					case 1: // load to RAM
						if(loadFile(file + (memory[4]<<8) + memory[5], memory[6], 512) == 1)
						{
							substate1 =  2;
						}
						break;
					case 2: // transmit Data
						if(sendMemory(512, memory[6]) == 1)
						{
							substate1 = 3;
						}
						break;
					case 3: // clear RAM
						for(int i = 0; i < memory[6]; i++)
						{
							memory[512 + i] = 0;
						}
						substate1 = 4;
						break;
					case 4: // send return message
						if(sendOk() == 1)
						{
							state = 0;
							substate1 = 0;
							messageClass = 0;
							file = 0;
							for(int i = 0; i < 6; i++)
							{
								memory[i] = 0;
							}
						}
						break;
					case 5: // send error
						if(sendError() == 1)
						{
							state = 0;
							substate1 = 0;
							messageClass = 0;
							file = 0;
							for(int i = 0; i < 6; i++)
							{
								memory[i] = 0;
							}
						}
						break;
					default:
						substate1 = 0;
						break;
					}
					break;
				case 3: // update
					switch(substate1)
					{
					case 0: // check command valid
						if(file == 0xE105) // E103 and E104 are read only
						{
							if(file == 0xE105)
								file = e105;

							substate1 = 1;
						}
						else
						{
							substate1 = 4;
						}
						break;
					case 1: // transmit to Memory
						if(saveMemory(7,memory[6],file + (memory[4]<<8)+memory[5]) == 1)
						{
							substate1 = 2;
						}
						break;
					case 2: // clear RAM
						{
						int l = memory[6] + 2;
						for(int i = 0; i < l; i++)
						{
							memory[i] = 0;
						}
						substate1 = 3;
						break;
						}
					case 3: // send return message
						if(sendOk() == 1)
						{
							state = 0;
							substate1 = 0;
							messageClass = 0;
							file = 0;
						}
						break;
					case 4:
						if(sendError() == 1)
						{
							state = 0;
							substate1 = 0;
							messageClass = 0;
							file = 0;
							int l = memory[6] + 2;
							for(int i = 0; i < l; i++)
							{
								memory[i] = 0;
							}
						}
						break;
					default:
						substate1 = 0;
						break;
					}
					break;
				case 4: // control
					switch(substate1)
					{
					case 0: // check command valid
						break;
					case 1: // perform operations
						break;
					default:
						substate1 = 0;
						break;
					}
					break;
				default:
					state = 0;
					messageClass = 0;
					break;
				}

				break;
			default:
				state = 0;
				break;
			}
			switch(sensorState)
			{
			case 0: // switched off
				break;
			case 1:
			default:
				break;
			}
		}
	}

private:
	double Uload;
	// MEM_SIZE
	BYTE memory[1024];
	unsigned int file;
	unsigned int memoryCounter;
	int e103;
	int e104;
	int e105;
	int e106;

};



#endif /* UC_H_ */

/*
 TransmissionQuartz.h (Timer for Receiver and Sender)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef TRANSMISSIONQUARTZ_H_
#define TRANSMISSIONQUARTZ_H_

#include "includes.h"

SC_MODULE(TransmissionQuartz)
{
public:
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clkin;
	sc_in<bool> enabled;
	sc_out<bool> clkout;

	SC_CTOR(TransmissionQuartz)
	{
		clock = false;
		ticks = 0;

		SC_THREAD(work);
		sensitive << clkin.pos();
	}

	void work()
	{
		while(true)
		{
			wait();
			if(enabled.read() == true && ticks++ >= Byterate424kBit / 2)
			{
				clkout.write(clock);
				clock = !clock;
				ticks = 0;
			}
		}
	}

private:
	bool clock;
	unsigned int ticks;

};



#endif /* TRANSMISSIONQUARTZ_H_ */

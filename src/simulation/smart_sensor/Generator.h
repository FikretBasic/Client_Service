/*
 * Generator.h (Sensor Values get inserted here)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef GENERATOR_H_
#define GENERATOR_H_

#include "includes.h"

SC_MODULE(Generator)
{
public:
	enum processes { sine, noise, sawtooth, triangle, square, constant, custom };
	processes proces;

	unsigned long long runs;
	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk;
	sc_in<bool> eclk;
	sc_out<double> load;
	sc_in<bool> reqData;
	sc_out<BYTE> data;
	sc_fifo<unsigned long> customData;

	SC_CTOR(Generator)
	{
		proces = custom;
		runs = 0;

		convert = 0;
		usage = 0;
		b = 0;
		hold = 0;
		PI = 3.141592;


		SC_THREAD(sense);
		sensitive << clk.pos();

		SC_THREAD(giveData);
		sensitive << clk.pos();

		SC_THREAD(physicalProcess);
		sensitive << eclk.pos();

	}
	void giveData()
	{
		while(1)
		{
			wait();
			usage += 1;
			if(b > 0 || reqData.read() == true)
			{
				usage += 1;
				if(b == 0)
				{
					for(int i = 0; i < 4; i++)
						omem[i] = mem[i];
				}
				data.write(omem[b]);
				if(b++ == 4)
					b = 0;
			}
		}
	}

	void sense()
	{
		while (true)
		{
			wait();
			if(convert++ > 18)
			{
				usage += 1;
				convert = 0;
				mem[0] = hold & 0x000000FF;
				mem[1] = hold & 0x0000FF00 >> 8;
				mem[2] = hold & 0x00FF0000 >> 16;
				mem[3] = hold & 0xFF000000 >> 24;
			}
		}
	}

private:
	int convert;
	double usage ;
	int b;
	BYTE omem[4];
	BYTE mem[4];
	unsigned long hold;
	double PI;

	void physicalProcess()
	{
		wait();
		while (true)
		{
			load.write(usage);
			usage = 0;
			switch (proces)
			{
			case sine:
				sineGenerator(runs);
				break;
			case noise:
				noiseGenerator();
				break;
			case sawtooth:
				sawtoothGenerator(runs);
				break;
			case triangle:
				triangleGenerator(runs);
				break;
			case square:
				squareGenerator(runs);
				break;
			case constant:
				constantGenerator();
				break;
			case custom:
				customOutput();
				break;
			default:
				break;
			}

			runs++;
			wait();
		}

	}

	void customOutput()
	{
		if(customData.num_available() > 0)
			customData.nb_read(hold);
	}

	void sineGenerator(long runs)
	{
		hold = (unsigned long)(0 + (1600.0 * sin(PI * double(runs) / 1000000000.0)));

	}

	void sawtoothGenerator(long runs)
	{
		hold = (unsigned long)((runs % 1000000000) * 50);
	}

	void triangleGenerator(long runs)
	{
		if (runs % 70 < 35)
			hold = (unsigned long)((runs % 500000000) * 100);
		else
			hold = (unsigned long)(7000 - (runs % 500000000) * 100);
	}

	void squareGenerator(long runs)
	{
		if (runs % 1000000000 < 500000000)
			hold = (unsigned long)(6000);
		else
			hold = (unsigned long)(0);
	}

	void noiseGenerator()
	{
		hold = (unsigned long)(2000 * rand());
	}

	void constantGenerator()
	{
		hold = (unsigned long)(12345);
	}
};



#endif /* GENERATOR_H_ */

/*
 * includes.cpp
 *
 *  Created on: 23 Mar 2018
 *      Author: its2016
 */

#include "includes.h"

/****************************************/
/*          global variables            */
/****************************************/
//SysC stuff

const unsigned int T_16384kHz = 61;
const unsigned int T_16MHz = T_16384kHz;
const unsigned int T_8192kHz = 122;
const unsigned int T_8MHz = T_8192kHz;
const unsigned int T_4096kHz = 244;
const unsigned int T_4MHz = T_4096kHz;
const unsigned int T_2048kHz = 488;
const unsigned int T_2MHz = T_2048kHz;
const unsigned int T_1024kHz = 976;
const unsigned int T_1MHz = T_1024kHz;
const unsigned int T_512kHz = 1953;
const unsigned int T_256kHz = 3906;
const unsigned int T_128kHz = 7812;
const unsigned int T_64kHz = 15625;
const unsigned int T_50kHz = 20000;
const unsigned int T_32kHz = 31250;
const unsigned int T_16kHz = 62500;
const unsigned int T_8kHz = 125000;
const unsigned int T_4kHz = 250000;
unsigned int SYS_CLK_PERIOD = T_1MHz;
const sc_time_unit TIME_BASE = SC_NS;
const unsigned long long ticksInH = 3600/(SYS_CLK_PERIOD)*1000*1000*1000;

int stepSize = 0;
sc_time_unit stepUnit = SC_NS;

//sensor specs
const size_t MEM_SIZE = 1024;
const size_t NVRAM_SIZE = 4096;
const double MAX_BATTERY_VOLTAGE = 4.5;
const double BATTERY_CAPACITY = 0.1; //Farad
const double BATTERY_RESISTANCE = 800; //Ohm
const double MAX_BATTERY_CHARGE = MAX_BATTERY_VOLTAGE * BATTERY_CAPACITY; // Q Culomb
const double INIT_BATTERY_VOLTAGE = 2.2;
const double INIT_BATTERY_CHARGE = INIT_BATTERY_VOLTAGE * BATTERY_CAPACITY;
//const double MAX_BATTERY_CHARGE = 138000000 * ticksInH; // 0,138mAh = 0,1F * 5V //[pAh] -> [pATicks]
//const double INIT_BATTERY_CHARGE = 100000;//50000000 * ticksInH;
const unsigned int Byterate848kBit = 1179;
const unsigned int Byterate424kBit = 2358;//18868 / 8;
const unsigned int Byterate212kBit = 4716;

const double ANTENNA_USE = 1;

//const double ENERGY_GAIN = 1.2057;
//message buffer
std::string bufnames[7]  = {"trace", "timeunit", "timestep", "commands", "sensordata", "proceed", "energy"};
std::string bufTokens[7] = {"TR"   , "TU"      , "TS"      , "CMD"     , "SD"        , "RUN"    , "E"};
pthread_mutex_t bufUse;
object* buffer = NULL;
//trace file
std::string filename = "";

//advance control
object* adv;
std::string advnames[3] = {"UC", "Send", "Recv"};


/**************************************/
/*   helper functions for debugging   */
/**************************************/

void printDebug(BYTE in)
{
	printf("<ddata>%02X<\\ddata>\n", in);
}

void printDebug(std::string in)
{
	std::cout << "<ddata>" << in << "<\\ddata>" << std::endl;
}

void sendToCallingSimulation(std::string in)
{
	std::cout << in << std::endl;
}


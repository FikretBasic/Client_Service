/*
 * Timer.h
 *
 *  Created on: Dec 14, 2017
 *      Author: ubuntu
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "includes.h"
#include "utils.h"

SC_MODULE(Timer)
{
public:

	unsigned long long ticksLeft;

	/*****************/
	/*    Modules    */
	/*****************/


	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> clk;
	sc_in<bool> start;
	sc_in<BYTE> ms;
	sc_out<bool> finished;

	SC_CTOR(Timer)
	{
		ticksLeft = 0;

		SC_THREAD(run);
		sensitive << clk.pos();

		SC_THREAD(setTimer)
		sensitive << clk.pos();

	}

	void run()
	{
		while(true)
		{
			wait();
			if(ticksLeft == 0)
			{
				finished.write(true);
			}
			else
			{
				ticksLeft--;
			}
		}
	}

	void setTimer()
	{
		while(true)
		{
			wait();
			if(start.read() == true)
			{
				ticksLeft = ms.read() * sc_time(1, TIME_BASE) / sc_time(1, SC_MS);
			}
		}
	}

private:


};

#endif /* TIMER_H_ */

#ifndef _BUFFER_H_
#define _BUFFER_H_

#include <iostream>
#include <string>

namespace gazebo{

struct Value{
	std::string value;
	struct Value* next;
};

typedef struct Value value;
struct Object{
	std::string name;
	std::string description;
	value* data;
	struct Object* next;
};
typedef struct Object object;

bool modifyObjectDescription(object** inbuffer, std::string name, std::string desc);

std::string getObjectDescription(object** inbuffer, std::string name);

bool appendBufferObject(object** inbuffer, std::string name);

bool appendBufferValue(object** inbuffer, std::string name, std::string ivalue);

std::string getBufferValue(object** inbuffer, std::string name, int val = 0);

bool modifyValue(object** inbuffer, std::string name, std::string ivalue, int val = 0);

std::string getBufferOValues(object** inbuffer, std::string name);

int getNumValuesOf(object** inbuffer, std::string name);

int getNumValues(object** inbuffer);

bool printBufferContents(object** inbuffer);

bool clearBufferValues(object** inbuffer, std::string excludes[] = NULL, int len = 0);

bool clearBufferValuesOf(object** inbuffer, std::string name);

bool clearBufferValue(object** inbuffer, std::string name, int val = 0);

bool deleteBufferObject(object** inbuffer, std::string name);

std::string getAndRemoveValue(object** inbuffer, std::string name, int value = 0);

bool deleteBuffer(object** inbuffer);

bool searchTokenAndAppend(object** inbuffer, std::string token, std::string name, std::string in);

bool searchAndAppendAllTokens(object** inbuffer, std::string in);

}
#endif //_BUFFER_H_

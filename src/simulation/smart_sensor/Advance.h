/*
 * Advance.h (Module to stop simulation prematurely)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef ADVANCE_H_
#define ADVANCE_H_

#include "includes.h"

SC_MODULE(Advance)
{
public:
	sc_time nextStop;
	/*****************/
	/*    Modules    */
	/*****************/
	Battery* pBatt;
	Generator* pGen;

	/*************/
	/*  Signals  */
	/*************/
	sc_in<bool> enabled;
	sc_in<bool> clk;
	sc_signal<bool> tAdvance;
	sc_signal<double> advanceTime;

	SC_CTOR(Advance)
	{
		pBatt = NULL;
		pGen = NULL;

		switchOffState = 0;
		substate = 0;

		SC_THREAD(work);
		sensitive << clk.neg();
	}
	void work()
	{
		tAdvance.write(false);
		while(true)
		{
			wait();
			if(switchOffState == 0 &&
					getBufferValue(&adv, advnames[0]) == "0" &&
					getBufferValue(&adv, advnames[1]) == "0" &&
					getBufferValue(&adv, advnames[2]) == "0")
			{
				switchOffState = 1;
				//std::cout << "<ddata>saveTime<\\ddata>" << std::endl;
			}
			if(enabled.read() == false && switchOffState == 0)
			{
				switchOffState = 1;
				substate = 9;
				//std::cout << "<ddata>noEnergy<\\ddata>" << std::endl;
			}

			switch(switchOffState)
			{
			case 0:
				advanceTime.write(0);
				substate = 0;
				break;
			case 1:
				if((getBufferValue(&adv, advnames[0]) != "0" ||
					getBufferValue(&adv, advnames[1]) != "0" ||
					getBufferValue(&adv, advnames[2]) != "0") &&
					enabled.read() == true)
				{
					switchOffState = 0;
					substate = 0;
				}
				if(substate++ > 10)
				{
					switchOffState = 2;
					substate = 0;
				}

				break;
			case 2:
				tAdvance.write(true);
				switchOffState = 3;
				advanceTime.write(0);

				break;
			case 3:
				tAdvance.write(false);
				if(pBatt)
				{
					double t = (nextStop - sc_time_stamp()).to_double();
					advanceTime.write(t);
					// the following causes waring w713 -> charge gets changed during battery operation and when it is reevaluated here
					pBatt->charge -= t / SYS_CLK_PERIOD * pBatt->lastload;
					if(pBatt->charge < 0)
						pBatt->charge = 0;
					if(pBatt->charge > MAX_BATTERY_CHARGE)
						pBatt->charge = MAX_BATTERY_CHARGE;
				}
				//else
				//{
				//	std::cout << "<ddata>Advance not connected to Battery<\\ddata>" << std::endl;
				//}

				//std::cout << "\nPgen: " << pGen << std::endl;

				if(pGen)
				{
					double t = (nextStop - sc_time_stamp()).to_default_time_units();
					pGen->runs += (unsigned long long)t;
				}
				//else
				//{
				//	std::cout << "<ddata>Advance not connected to Generator<\\ddata>" << std::endl;
				//}

				clearBufferValuesOf(&adv, advnames[0]);
				appendBufferValue(&adv, advnames[0], "1");

				switchOffState = 0;
				sc_pause();
				break;
			default:
				// should not happen
				switchOffState = 0;
				break;
			}
		}
	}

private:
	int switchOffState;
	int substate;


};



#endif /* ADVANCE_H_ */

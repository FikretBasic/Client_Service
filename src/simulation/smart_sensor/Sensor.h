/*
 * Sensor.h (Top level module - connects the parts)
 *
 *  Created on: May 18, 2017
 *      Author: ubuntu
 */

#pragma once

#ifndef SENSOR_H_
#define SENSOR_H_

#include "includes.h"

SC_MODULE(Sensor)
{
public:
	/*****************/
	/*    Modules    */
	/*****************/
	Battery* Batt;
	SystemQuartz* SysQ;
	TransmissionQuartz* TrmQ;
	Antenna* Ant;
	Receiver* Recv;
	Sender* Send;

	UC* uc;
	Memory* Mem;

	//Timer* Tim = new Timer("Tim");

	Generator* Gen;
	Advance* Adv;

	/*************/
	/*  Signals  */
	/*************/
	//loads
	sc_signal<double> sensorLoad;
	sc_signal<double> energyIn;
	sc_signal<double> antennaLoad;
	sc_signal<double> receiverLoad;
	sc_signal<double> UCLoad;
	sc_signal<double> senderLoad;
	sc_signal<double> memoryLoad;
	sc_signal<double> generatorLoad;

	sc_signal<double> voltage;

	//status info and clocks
	sc_in<bool> clkIn;
	sc_signal<bool> sysClk;
	sc_signal<bool> trmClk;
	sc_signal<bool> energyAvailable;

	//data transmissions
	sc_signal<BYTE> antennaOut;
	sc_signal<BYTE> recvOut;
	sc_signal<BYTE> UCOut;

	sc_signal<unsigned int> memoryAddress;
	sc_signal<BYTE> dataToMemory;
	sc_signal<BYTE> dataFromMemory;

	sc_signal<BYTE> dataSensorToUC;

	//control lines
	sc_signal<bool> dataAvailable;
	sc_signal<bool> dataReady;
	sc_signal<bool> dataToUC;
	sc_signal<bool> inputPending;
	sc_signal<bool> transmitData;
	sc_signal<bool> writeToMemory;
	sc_signal<bool> getDataFromSensor;


	SC_CTOR(Sensor)
	{
		Batt = new Battery("Battery");
		SysQ = new SystemQuartz("SysQ");
		TrmQ = new TransmissionQuartz("TrmQ");
		Ant = new Antenna("Ant");
		Recv = new Receiver("Recv");
		Send = new Sender("Send");

		uc = new UC("uc");
		Mem = new Memory("Mem");

		//Tim = new Timer("Tim");

		Gen = new Generator("Gen");
		Adv = new Advance("Adv");


		Batt->clk(clkIn); // needs to be updated at all times
		Batt->load(sensorLoad);
		Batt->enabled(energyAvailable);
		Batt->voltage(voltage);

		SysQ->enabled(energyAvailable);
		SysQ->clkin(clkIn);
		SysQ->clkout(sysClk);

		TrmQ->enabled(energyAvailable);
		TrmQ->clkin(clkIn);
		TrmQ->clkout(trmClk);

		Adv->pBatt = Batt;
		Adv->pGen = Gen;
		Adv->clk(clkIn);
		Adv->enabled(energyAvailable);

		Ant->clk(trmClk);
		Ant->eclk(clkIn);
		Ant->data(antennaOut);
		Ant->energy(energyIn);
		Ant->load(antennaLoad);
		Ant->save(dataAvailable);
		Ant->dataFinished(inputPending);

		Recv->clk(sysClk);
		Recv->dataIn(antennaOut);
		Recv->dataAvailable(dataAvailable);
		Recv->dataOut(recvOut);
		Recv->ready(dataReady);
		Recv->sendData(dataToUC);
		Recv->inputFinished(inputPending);
		Recv->Rload(receiverLoad);

		Send->clk(sysClk);
		Send->Sclk(trmClk);
		Send->send(transmitData);
		Send->data(UCOut);
		Send->load(senderLoad);

		uc->clk(sysClk);
		uc->receiveData(dataToUC);
		uc->dataAvailable(dataReady);
		uc->dataFromReceiver(recvOut);
		uc->dataToTransmitter(UCOut);
		uc->sendData(transmitData);
		uc->load(UCLoad);
		uc->memoryAddress(memoryAddress);
		uc->dataToMemory(dataToMemory);
		uc->dataFromMemory(dataFromMemory);
		uc->memoryWrite(writeToMemory);
		uc->loadSensorData(getDataFromSensor);
		uc->sensorData(dataSensorToUC);

		Mem->clk(sysClk);
		Mem->addr(memoryAddress);
		Mem->write(writeToMemory);
		Mem->dataIn(dataToMemory);
		Mem->dataOut(dataFromMemory);
		Mem->energy(memoryLoad);

		Gen->eclk(clkIn);	// displays the external process - needs to be updated
		Gen->clk(sysClk);
		Gen->data(dataSensorToUC);
		Gen->reqData(getDataFromSensor);
		Gen->load(generatorLoad);

		Gen->proces = Gen->sine;

		SC_THREAD(addLoads);
		sensitive << clkIn;
	}

	void addLoads()
	{
		while(true)
		{
			wait();
			double load = 0;
			load -= energyIn.read();
			load += antennaLoad.read();
			load += receiverLoad.read();
			load += UCLoad.read();
			load += senderLoad.read();
			load += memoryLoad.read();
			load += generatorLoad.read();
			sensorLoad.write(load);

			//std::cout << "Load: " << load << std::endl;
		}
	}

private:


};


#endif /* SENSOR_H_ */

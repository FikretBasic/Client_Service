/*
 * SmartSensorTop.h
 *
 *  Created on: 21 Mar 2018
 *      Author: its2016
 */

#include "Simulation.h"
#include "smart_sensor/includes.h"
#include "smart_sensor/utils.h"

#ifndef SIMULATION_SMARTSENSORTOP_H_
#define SIMULATION_SMARTSENSORTOP_H_

namespace simc {

class SmartSensorTop : public Simulation
{
public:
	SmartSensorTop();
	virtual ~SmartSensorTop();

	void initializeSimulation();
	void setTraceName(uint16_t connectionCode);
	void defineTraceOption(std::string option);
	std::vector<std::string> getTraceOptions();
	void setSimulationParameter(std::string param, int position);
	void closeTrace();

	Sensor* getSensor();

private:
	Sensor *pSensor;
	sc_core::sc_clock *clock;
};

} /* namespace simc */

#endif /* SIMULATION_SMARTSENSORTOP_H_ */

/*
 * Counter.cpp
 *
 *  Created on: 13 Mar 2018
 *      Author: its2016
 */

#include "Counter.h"

void counter::testProgram()
{
  if (reset.read())
  {
	  count.write(0);
  }
  else if (enable.read())
  {
	  count.write(count.read() + 1);
	  //std::cout << "Value: " << count.read() << std::endl;
	  counter_out.write(count.read());
  }
}

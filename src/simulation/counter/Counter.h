/*
 * Counter.h
 *
 *  Created on: 13 Mar 2018
 *      Author: its2016
 */

#include "systemc.h"

#ifndef SIMULATION_COUNTER_COUNTER_H_
#define SIMULATION_COUNTER_COUNTER_H_

SC_MODULE (counter)
{
  sc_in<bool>   clock ;      // Clock input of the design
  sc_in<bool>   reset ;      // active high, synchronous Reset input
  sc_in<bool>   enable;      // Active high enable signal for counter
  sc_out<sc_uint<4> > counter_out; // 4 bit vector output of the counter

  SC_CTOR (counter)
  {
	  SC_METHOD(testProgram);
	  sensitive << clock.pos();
  };

  void testProgram();

  sc_signal<sc_uint<4 > > count;

};

#endif /* SIMULATION_COUNTER_COUNTER_H_ */

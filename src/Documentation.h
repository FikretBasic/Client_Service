/*
 * Documentation.h
 *
 *  Created on: 13 Mar 2018
 *      Author: its2016
 */

#ifndef DOCUMENTATION_H_
#define DOCUMENTATION_H_

/**
 *
 *	\mainpage Server application for handling Multiple Client interactions
 *
 *	\author Fikret Basic
 *	\version 0.5
 *	\date 25.02.2018.
 *
 *	\copyright Technical University of Graz - All Rights Reserved
 *
 *
 *	\section intent_sec Overview of the Application
 *
 *	The Application was developed in Eclipse both for Windows and Linux operating system.
 *
 *	\section compile_sec Running the Application
 *	The Application can be run both on the Windows and Linux system. It is only necessary to compile it. \n
 *	After that in "Debug" folder lies the executable or the binary file which can then be run though the terminal command. \n
 *	The program accepts up to two arguments:
 *	- \b arg1 : the server address
 *	- \b arg2 : the connection port
 *
 *	If no argument is given, the default one will be used (for the address 127.0.0.1, and port 8888).
 *
 */



#endif /* DOCUMENTATION_H_ */

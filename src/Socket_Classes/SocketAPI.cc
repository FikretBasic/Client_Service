/*
 * SocketAPI.cpp
 *
 *  Created on: 21 Feb 2018
 *      Author: its2016
 */

#include "SocketAPI.h"

#if defined (_WIN32)

void setErrorAndExit(const char *errorMessage)
{
	fprintf(stderr,"%s: %d\n", errorMessage, WSAGetLastError());
	exit(1);
}

void setError(const char *errorMessage)
{
	fprintf(stderr,"%s: %d\n", errorMessage, WSAGetLastError());
}

#else

void setErrorAndExit(const char *errorMessage)
{
	perror(errorMessage);
	exit(1);
}

void setError(const char *errorMessage)
{
	perror(errorMessage);
}

#endif


struct addrinfo* makeAddrInfo(const char *addr, const char *port)
{
	struct addrinfo tempInfo;
	struct addrinfo *returnInfo;

	memset(&tempInfo, 0, sizeof tempInfo);

	tempInfo.ai_family = AF_UNSPEC;			// don't care IPv4 or IPv6
	tempInfo.ai_socktype = SOCK_STREAM;		// TCP stream sockets

	const char *temp_port;

	// If no port is specified, take a constant one. Not needed for the address.
	if(port == NULL)
	{
		temp_port = "8888";
	}
	else
	{
		temp_port = port;
	}

	if(addr == NULL)
	{
		tempInfo.ai_flags = AI_PASSIVE;		// fill in my IP for me
	}

	if(getaddrinfo(addr, temp_port, &tempInfo, &returnInfo) == S_ERROR)
	{
		setErrorAndExit("Get address info error: ");
	}

	return returnInfo;
}

int shutdownOp(int socketID, int flags)
{
	if( socketID > -1 && ( flags > -1 && flags < 3) )
	{
		if(shutdown(socketID, flags) < 0)
		{
			setError("Shutdown procedure: ");
			return S_ERROR;
		}
		else
		{
			return SUCCESS;
		}
	}
	else
	{
		return S_ERROR;
	}
}

int receive(int socketID, char *buffer, size_t len, int flags)
{
	if (socketID > -1 && len > 0)
	{
		int tempFlags;
		int recvOut;

		if(flags < 0)
			tempFlags = 0;
		else
			tempFlags = flags;

		recvOut = recv(socketID, buffer, len, tempFlags);

		if(recvOut == S_ERROR)
		{
			setError("Receive error: ");
			return S_ERROR;
		}
		else
		{
			return recvOut;
		}
	}
	else
	{
		return S_ERROR;
	}
}

int sendTo(int socketID, const char *buffer, size_t len, int flags)
{
	if (socketID > -1 && len > 0)
	{
		int tempFlags;
		if(flags < 0)
			tempFlags = 0;
		else
			tempFlags = flags;

		if(send(socketID, buffer, len, tempFlags) == S_ERROR)
		{
			setError("Sender error: ");
			return S_ERROR;
		}
		else
		{
			return SUCCESS;
		}
	}
	else
	{
		return S_ERROR;
	}
}

int socketConnect(struct addrinfo* addr, int errorFlag)
{
	int socketID;
	if ((socketID = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol)) == S_ERROR)
	{
		if (errorFlag == 1)
		{
			setErrorAndExit("Establish socket: ");
		}
		else
		{
			setError("Establish socket: ");
			return S_ERROR;
		}
	}
	return socketID;
}

int socketBind(int socketID, struct addrinfo* addr, int errorFlag)
{
	if (bind(socketID, addr->ai_addr, addr->ai_addrlen) == S_ERROR)
	{
		closeSocket(socketID);
		if (errorFlag == 1)
		{
			setErrorAndExit("Binding server: ");
		}
		else
		{
			setError("Binding server: ");
			return S_ERROR;
		}
	}
	return SUCCESS;
}

int clientConnect(int socketID, struct addrinfo* addr, int errorFlag)
{
	if (connect(socketID, addr->ai_addr, addr->ai_addrlen) == S_ERROR)
	{
		closeSocket(socketID);
		if (errorFlag == 1)
		{
			setErrorAndExit("Connect client: ");
		}
		else
		{
			setError("Connect client: ");
			return S_ERROR;
		}
	}
	return SUCCESS;
}

int socketListen(int socketID, int pendingConnections, int errorFlag)
{
	if(listen(socketID, pendingConnections) == S_ERROR)
	{
		if(errorFlag == 1)
		{
			setErrorAndExit("Server listen connection: ");
		}
		else
		{
			setError("Server listen connection: ");
		}
	}
	return SUCCESS;
}


int acceptNewClient(int socketID, struct sockaddr_storage *clientAddr)
{
	struct sockaddr_storage clientAddr2;

	socklen_t sinSize;
	sinSize = sizeof clientAddr2;
	int clientSockID;

	clientSockID = accept(socketID, (struct sockaddr *)&clientAddr2, &sinSize);
	*clientAddr = clientAddr2;

	return clientSockID;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

uint16_t get_in_port(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return ((struct sockaddr_in*)sa)->sin_port;
    }

    return ((struct sockaddr_in6*)sa)->sin6_port;
}

void getIPAddrString(struct sockaddr_storage *clientAddr, char* buffer, size_t length)
{
    inet_ntop(clientAddr->ss_family,
        get_in_addr((struct sockaddr *)clientAddr),
		buffer, length);
}

uint16_t getPortString(struct sockaddr_storage *clientAddr)
{
	return ntohs(get_in_port((struct sockaddr *)clientAddr));
}

int socketOptReusePort(int socketID)
{
	#if defined (_WIN32)
		const char* val = "";
	#else
		int valTemp = 1;
		int *val = &valTemp;
	#endif

	if (setsockopt(socketID, SOL_SOCKET, SO_REUSEADDR, val, sizeof(int)) == S_ERROR)
	{
		setErrorAndExit("Option setting socket: ");
	}
	return SUCCESS;
}


int checkAndTerminate(struct addrinfo* addr)
{
	if(addr == NULL)
	{
		setErrorAndExit("Server has failed to bind: ");
	}
	return SUCCESS;
}

#if !defined (_WIN32)

void sigchld_handler(int s)
{
    // waitpid() might overwrite errno, so we save and restore it:
    int saved_errno = errno;
    while(waitpid(-1, NULL, WNOHANG) > 0);
	errno = saved_errno;
}

int reapZombieProcesses()
{
	struct sigaction sa;

    sa.sa_handler = sigchld_handler; // reap all dead processes
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == S_ERROR)
    {
        perror("sigaction: ");
        exit(1);
    }

    return SUCCESS;
}

#endif


void closeSocket(int id)
{
#if defined (_WIN32)
	closesocket(id);
#else
	close(id);
#endif
}


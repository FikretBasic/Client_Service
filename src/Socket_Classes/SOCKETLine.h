/*
 * SOCKETLine.h
 *
 *  Created on: 13 Mar 2018
 *      Author: its2016
 */

#include "../Includes.h"
#include "NETAddr.h"

#ifndef SOCKET_CLASSES_SOCKETLINE_H_
#define SOCKET_CLASSES_SOCKETLINE_H_

namespace server {

class SOCKET_Line {
public:
	SOCKET_Line();

	SOCKET_Line(SOCKET s);

	SOCKET_Line(NET_Addr *address, uint16_t connectionCode = 0);

	virtual ~SOCKET_Line();

	/**
	 * \brief Set particular socket identifier (change with the current one)
	 * \param s - socket identifier
	 */
	void setSocket(SOCKET s);

	/**
	 * \brief Set particular connection Code (change with the current one)
	 * \param connectionCode - connection Code to set
	 */
	void setConnectionCode(uint16_t connectionCode);

	/**
	* \brief Get saved Socket
	* \return SOCKET - socket identifier
	*/
	SOCKET getSocket() const;

	/**
	* \brief Get saved connection Code
	* \return uint16_t - connection Code
	*/
	uint16_t getConnectionCode();

	/**
	 * \brief Shutdown the saved socket based on the flag
	 * \param flags - flag which describes the shutdown procedure
	 */
	int shutdownSocket(int flags = 0);

	/**
	 * \brief Receive data at the other end of the socket
	 * \param buffer - char pointer to the received data
	 * \param len - maximum length of the received data
	 * \param flags - option descriptors
	 * \return int - status of the function
	 */
	int recv(char *buffer, size_t len, int flags = 0);

	/**
	 * \brief Send data at the other end of the socket
	 * \param buffer - char pointer to the sent data
	 * \param len - maximum length of the sent data
	 * \param flags - option descriptors
	 * \return int - status of the function
	 */
	int send(const char *buffer, size_t len = 0, int flags = 0);

	/**
	 * \brief Inner function used to write out the Server IP information
	 * \param addr - server address data
	 */
	void printHostInfo(struct addrinfo *addr);


private:
	SOCKET socket;				/**< unique socket identifier */
	uint16_t connectionCode;	/**< unique connection Code */
};

} /* namespace server */

#endif /* SOCKET_CLASSES_SOCKETLINE_H_ */

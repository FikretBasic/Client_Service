/*
 * SOCKETLine.cpp
 *
 *  Created on: 13 Mar 2018
 *      Author: its2016
 */

#include "SOCKETLine.h"

namespace server {

SOCKET_Line::SOCKET_Line()
{
	this->socket = -1;
	this->connectionCode = 0;
}

SOCKET_Line::SOCKET_Line(SOCKET s)
{
	this->socket = s;
	this->connectionCode = 0;
}

SOCKET_Line::SOCKET_Line(NET_Addr *address, uint16_t connectionCode)
{
	this->connectionCode = connectionCode;

	struct addrinfo *iter;

	for (iter = address->getServerInfo(); iter != NULL; iter = iter->ai_next)
	{
		this->socket = socketConnect(iter, 0);
		if(this->socket == -1)
			continue;

		if(clientConnect(this->socket, iter, 0) == -1)
			continue;

		this->printHostInfo(iter);

		break;
	}

	checkAndTerminate(iter);
	Stylist::instance()->printMessage("[+]Connected to Server.", M_INFO);
}

SOCKET_Line::~SOCKET_Line()
{
	closeSocket(this->socket);
	#if defined(_WIN32)
		WSACleanup();
	#endif
}

void SOCKET_Line::setSocket(SOCKET s)
{
	this->socket = s;
}

SOCKET SOCKET_Line::getSocket() const
{
	return this->socket;
}

void SOCKET_Line::setConnectionCode(uint16_t connectionCode)
{
	this->connectionCode = connectionCode;
}

uint16_t SOCKET_Line::getConnectionCode()
{
	return this->connectionCode;
}

int SOCKET_Line::shutdownSocket(int flags)
{
	return shutdownOp(this->socket, flags);
}

int SOCKET_Line::recv(char *buffer, size_t len, int flags)
{
	return receive(this->socket, buffer, len, flags);
}

int SOCKET_Line::send(const char *buffer, size_t len, int flags)
{
	if (len == 0)
	{
		return sendTo(this->socket, buffer, strlen(buffer), flags);
	}
	else
	{
		return sendTo(this->socket, buffer, len, flags);
	}
}

void SOCKET_Line::printHostInfo(struct addrinfo *addr)
{
	char message[200];
	strcpy(message, "[CLIENT] [");
	strcat(message, getCurrentTime());
	strcat(message, "] Socket of the Client side is successfully set on: ");
	strcat(message, inet_ntoa(((struct sockaddr_in *)addr->ai_addr)->sin_addr));

	Stylist::instance()->printMessage(message, M_INFO);
}

} /* namespace server */

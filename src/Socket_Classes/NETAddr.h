/*
 * NETAddr.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#include "../Includes.h"

#ifndef NETADDR_H_
#define NETADDR_H_

namespace server {

/**
 * \brief Class which holds and manipulates with the host address
 *
 * Provides the address functionality with the 'C' Socket functions through the Wrapper Facade design pattern.
 */

class NET_Addr
{
public:

	/**
	 * \brief Constructor, establish default address and port
	 */
	NET_Addr();

	/**
	 * \brief Initialisation Constructor with its own address and port
	 * \param addr - specific IP address
	 * \param port - specific port number
	 */
	NET_Addr(const char *addr, const char *port);

	/**
	 * \brief Destructor, frees the address
	 */
	virtual ~NET_Addr();

	sockaddr* getSockAddr() const ;
	socklen_t getSockLength() const ;
	char* getCannonicalName() const ;
	int getConFamily() const;
	int getFlags() const;
	int getProtocol() const;
	int getSocketType() const;
	size_t size() const;

	/**
	 * \brief Get complete addrinfo structure of the inner instance
	 * \return Pointer to the structure of the addrinfo
	 */
	addrinfo* getServerInfo();

	/**
	 * \brief Get complete addrinfo structure of the element that follows
	 * \return Return the next structure element in the list
	 */
	addrinfo* nextElement();

	/**
	 * \brief Inner function which writes out the current address and port
	 */
	void writeOutAddress();

private:
	struct addrinfo *serverInfo;		/**< list of pointers which hold the address information */
};

} /* namespace server */

#endif /* NETADDR_H_ */

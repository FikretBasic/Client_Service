/*
 * SOCKETHandler.cpp
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */


#include "SOCKETHandler.h"

namespace server {

SOCKET_Handler::SOCKET_Handler(SOCKET_Line &socc_line) : socc_line(socc_line)
{
	this->simHandler = new simc::CounterHandler();
}

SOCKET_Handler::SOCKET_Handler(SOCKET_Line &socc_line, simc::SimulationHandler *simHandler) : socc_line(socc_line)
{
	this->simHandler = simHandler;
}

// Not necessary, since the constructors of initiated objects will close down connections automatically
SOCKET_Handler::~SOCKET_Handler() {}

void SOCKET_Handler::startActivity(const char* actType)
{
	if (strcmp(actType, "echo") == 0)
	{
		this->startActivityEcho();
	}
	else
	{
		this->startActivitySimulation();
	}
}

void SOCKET_Handler::startActivityEcho()
{

	char buffer[1024];

	SOCKET clientSocket = socc_line.getSocket();

	while (1)
	{
		printf("Client: \t");
		fgets(buffer, sizeof buffer, stdin);
		socc_line.send(buffer, (int)strlen(buffer) - 1, 0);

		#if defined(_WIN32)
			Sleep(1000);
		#else
			sleep(1);
		#endif

		if (strcmp(buffer, ":exit\n") == 0)
		{
			closeSocket(clientSocket);

			Stylist::instance()->printMessage("[-]Disconnected from server.", M_ERROR);
			exit(1);
		}

		memset(buffer, '\0', sizeof(buffer));

		if (recv(clientSocket, buffer, 1024, 0) < 0)
		{
			Stylist::instance()->printMessage("[-]Error in receiving data.", M_ERROR);
		}
		else
		{
			std::string serverMessage = "[SERVER] [" + Str(getCurrentTime()) + "]: " + buffer;
			Stylist::instance()->printMessage(serverMessage.c_str(), M_INFO);
		}
	}
}

void SOCKET_Handler::startActivitySimulation()
{
	char buffer[1024];
	std::string statusMessage;

	SOCKET clientSocket = socc_line.getSocket();
	int instructionCounter = 0;

	while (1)
	{
		statusMessage.clear();

		//strcpy(buffer, messageSetter(instructionCounter));
		strcpy(buffer, simHandler->messageSetter(instructionCounter, socc_line.getConnectionCode()));

		statusMessage = "Client: " + Str(buffer);
		Stylist::instance()->printMessage(statusMessage.c_str(), M_INFO);

		socc_line.send(buffer, (int)strlen(buffer), 0);

		#if defined(_WIN32)
			Sleep(1000);
		#else
			sleep(1);
		#endif

		if (strcmp(buffer, "<t>status</t><d>connection_end</d>") == 0)
		{
			closeSocket(clientSocket);

			Stylist::instance()->printMessage("[-]Disconnected from server.", M_ERROR);
			break;
			//exit(1);
		}
		else if (strcmp(buffer, "<t>status</t><d>sim_started</d>") == 0)
		{
			//sc_start(this->stepSize, this->stepUnit);
			simHandler->startSimulation();
		}

		memset(buffer, '\0', sizeof(buffer));

		if (recv(clientSocket, buffer, 1024, 0) < 0)
		{
			Stylist::instance()->printMessage("[-]Error in receiving data.", M_ERROR);
		}
		else
		{
			statusMessage = "[SERVER] [" + Str(getCurrentTime()) + "]: " + buffer;
			Stylist::instance()->printMessage(statusMessage.c_str(), M_WARNING);

			//instructionCounter = messageInterpreter(buffer);
			instructionCounter = simHandler->messageInterpreter(buffer);
		}
	}

	//delete simHandler;
}


} /* namespace server */




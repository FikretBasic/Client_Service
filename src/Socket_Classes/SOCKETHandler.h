/*
 * SOCKETHandler.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

/// \todo [DONE - 08.03.2018.] Include the Strategy Design Pattern or similar to handle different server activities.

#include "SOCKETLine.h"
#include "../Includes.h"
#include "../simulation/sim_handler/CounterHandler.h"

#ifndef SOCKETHANDLER_H_
#define SOCKETHANDLER_H_

namespace server {

/**
 * \brief Handles a particular Socket communication (Server - Client)
 */

class SOCKET_Handler
{
public:

	/**
	 * \brief Initialising constructor which handles a particular socket connection and communication
	 * \param socc_line - socket connection object instance
	 */
	SOCKET_Handler(SOCKET_Line &socc_line);

	SOCKET_Handler(SOCKET_Line &socc_line, simc::SimulationHandler* simHandler);

	/**
	 * \brief Empty destructor, connection and communication automatically close the connection from their side
	 */
	virtual ~SOCKET_Handler();

	/**
	 * \brief Main function to start the interaction between the Server and the Client
	 */
	void startActivity(const char* actType);

	void startActivityEcho();

	void startActivitySimulation();

private:
	SOCKET_Line &socc_line;			/**< Instance of the Socket connection and communication */
	simc::SimulationHandler* simHandler;

	/**
	 * \brief Empty constructor, to hide from the possibility to initialise it outside
	 */
	SOCKET_Handler();

};

} /* namespace server */

#endif /* SOCKETHANDLER_H_ */

/*
 * NETAddr.cpp
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#include "NETAddr.h"

namespace server {

NET_Addr::NET_Addr()
{
	serverInfo = makeAddrInfo(NULL, NULL);
}

NET_Addr::NET_Addr(const char *addr, const char *port)
{
	serverInfo = makeAddrInfo(addr, port);
}

NET_Addr::~NET_Addr()
{
	freeaddrinfo(serverInfo);
}

sockaddr* NET_Addr::getSockAddr() const
{
	return this->serverInfo->ai_addr;
}

socklen_t NET_Addr::getSockLength() const
{
	return this->serverInfo->ai_addrlen;
}

char* NET_Addr::getCannonicalName() const
{
	return this->serverInfo->ai_canonname;
}

int NET_Addr::getConFamily() const
{
	return this->serverInfo->ai_family;
}

int NET_Addr::getFlags() const
{
	return this->serverInfo->ai_flags;
}

int NET_Addr::getProtocol() const
{
	return this->serverInfo->ai_protocol;
}

int NET_Addr::getSocketType() const
{
	return this->serverInfo->ai_socktype;
}

size_t NET_Addr::size() const
{
	return sizeof (this->serverInfo);
}


addrinfo* NET_Addr::getServerInfo()
{
	return this->serverInfo;
}

addrinfo* NET_Addr::nextElement()
{
	return this->serverInfo->ai_next;
}

} /* namespace server */

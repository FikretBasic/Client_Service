/*
 * HelperFunctions.cpp
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "HelperFunctions.h"

#if defined (_WIN32)

std::string getCurrentTime()
{
	SYSTEMTIME lt;
	GetLocalTime(&lt);
	std::string timeString = Str(lt.wHour) + ":" + Str(lt.wMinute) + ":" + Str(lt.wSecond);

	return timeString;
}

#else

const char* getCurrentTime()
{
	time_t currentTime;
	struct tm *localTime_ = NULL;
	time(&currentTime);
	localTime_ = localtime(&currentTime);

	std::string timeString = Str(localTime_->tm_hour) + ":" + Str(localTime_->tm_min) + ":" + Str(localTime_->tm_sec);
	return timeString.c_str();
}

#endif

std::string get_str_between_two_str(const std::string &s,
        const std::string &start_delim,
        const std::string &stop_delim)
{
    unsigned first_delim_pos = s.find(start_delim);
    unsigned end_pos_of_first_delim = first_delim_pos + start_delim.length();
    unsigned last_delim_pos = s.find(stop_delim);

    return s.substr(end_pos_of_first_delim,
            last_delim_pos - end_pos_of_first_delim);
}

std::string getInput()
{
	std::string o = "";
	char c;
	std::cin.get(c);
	while(c != '\n')
	{
		o += c;
		std::cin.get(c);
	}
	return o;
}

simc::Simulation* factoryMethodSimulation(uint16_t connectionCode)
{
	uint8_t leadingVal = connectionCode / 1000;

	// Add additional inputs for additional simulations

	if (leadingVal == 1)
	{
		Stylist::instance()->printMessage("Hi! You have chosen the following simulation: Counter", M_INFO);
		return new simc::CounterTop;
	}
	else if (leadingVal == 2)
	{
		Stylist::instance()->printMessage("Hi! You have chosen the following simulation: Smart Sensor", M_INFO);
		return new simc::SmartSensorTop;
	}
	else
	{
		Stylist::instance()->printMessage("Hi! You have chosen the wrong connection code. The program aborts!", M_ERROR);
		exit(1);
	}
}

simc::SimulationHandler* factoryMethodSimulationHandler(uint16_t connectionCode)
{
	uint8_t leadingVal = connectionCode / 1000;

	if(leadingVal == 1)
	{
		return new simc::CounterHandler;
	}
	else if (leadingVal == 2)
	{
		return new simc::SmartSensorHandler;
	}
	else
	{
		return new simc::SmartSensorHandler;
	}
}

std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}




/*
 * SimplePacket.cpp
 *
 *  Created on: 14 Mar 2018
 *      Author: its2016
 */

#include "SimplePacket.h"

SimplePacket::SimplePacket()
{
	this->type = "";
	this->data = "";
}

SimplePacket::SimplePacket(char* message)
{
	setMessage(message);
}

SimplePacket::~SimplePacket() {}


void SimplePacket::setMessage(char *message)
{
	this->type = get_str_between_two_str(Str(message), "<t>", "</t>");
	this->data = get_str_between_two_str(Str(message), "<d>", "</d>");
}

std::string SimplePacket::getType()
{
	return this->type;
}

std::string SimplePacket::getData()
{
	return this->data;
}


/*
 * HelperFunctions.h
 *
 *  Created on: 8 Mar 2018
 *      Author: its2016
 */

#include "../Includes.h"
#include "../simulation/CounterTop.h"
#include "../simulation/SmartSensorTop.h"
#include "../simulation/sim_handler/CounterHandler.h"
#include "../simulation/sim_handler/SmartSensorHandler.h"

#ifndef TOOLS_HELPERFUNCTIONS_H_
#define TOOLS_HELPERFUNCTIONS_H_

#if defined(_WIN32)
std::string getCurrentTime();
#else
 /**
 * \brief Get current system time (timestamp) in HH:MM:SS format
 * \return const char* - char array which holds the current time
 */
const char* getCurrentTime();
#endif

std::string get_str_between_two_str(const std::string &s,
        const std::string &start_delim,
        const std::string &stop_delim);

std::string getInput();

simc::Simulation* factoryMethodSimulation(uint16_t connectionCode);
simc::SimulationHandler* factoryMethodSimulationHandler(uint16_t connectionCode);


template<typename Out>
void split(const std::string &s, char delim, Out result)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
    	if(!item.empty())
    		*(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim);

#endif /* TOOLS_HELPERFUNCTIONS_H_ */

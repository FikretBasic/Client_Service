/*
 * Stylist.h
 *
 *  Created on: 26 Feb 2018
 *      Author: its2016
 */

#include "../Includes.h"

#ifndef TOOLS_STYLIST_H_
#define TOOLS_STYLIST_H_

/**
 * Message type used to describe the output (colour) of the specific text
 */
enum MessageType
{
	M_INFO
	, M_WARNING
	, M_ERROR
};

/**
 * \brief Class intended for use for providing coloured and stylised terminal text output
 *
 * Made with Singleton design pattern (only one instance, globally accessible), and Facade design pattern
 * for cross-platform support.
 */

class Stylist {
public:

	/**
	 * \brief Destructor of the Stylist class
	 */
	virtual ~Stylist();

	/**
	 * \brief Return the only inner instance of the Stylist class (or create before return)
	 * \return Stylist* - dynamic instance of the Stylist class
	 */
	static Stylist* instance();

	/**
	 * \brief Print prescribed message out to the console
	 * \param message - specific message
	 * \param MessageType - one of the three message types (sp. colour)
	 * \param opt - additional console text options
	 */
	void printMessage(const char* message, MessageType type, int opt = 1);

private:

	/**
	 * \brief Constructor of the Stylist class, hidden to be only called from inside the class
	 */
	Stylist();

	static Stylist *instance_t;		/**< Unique Class instance */

	#if defined(_WIN32)
		HANDLE  hConsole;	/**< Used by Windows for console handling */
	#endif
};


#endif /* TOOLS_STYLIST_H_ */

/*
 * Stylist.cpp
 *
 *  Created on: 26 Feb 2018
 *      Author: its2016
 */

#include "Stylist.h"

Stylist* Stylist::instance_t = 0;

Stylist::Stylist()
{
	#if defined(_WIN32)
		hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	#endif
}

Stylist::~Stylist()
{
	delete this->instance_t;
}

Stylist* Stylist::instance()
{
    if (!instance_t)
    	instance_t = new Stylist();
    return instance_t;
}

#if defined (_WIN32)

void Stylist::printMessage(const char* message, MessageType type, int opt)
{
	//std::cout << "\033[1;31mbold red text\033[0m\n";
	if (type == M_INFO)
	{
	    SetConsoleTextAttribute(hConsole, 11);
	}
	else if (type == M_WARNING)
	{
	    SetConsoleTextAttribute(hConsole, 14);
	}
	else if (type == M_ERROR)
	{
	    SetConsoleTextAttribute(hConsole, 12);
	}
	else
	{
	    SetConsoleTextAttribute(hConsole, 15);
	}

    std::cout << message << std::endl;
    SetConsoleTextAttribute(hConsole, 15);
}

#else

void Stylist::printMessage(const char* message, MessageType type, int opt)
{
	//std::cout << "\033[1;31mbold red text\033[0m\n";
	if (type == M_INFO)
	{
		std::cout << "\033[" << opt << ";36m" << message << "\033[0m" << std::endl;
	}
	else if (type == M_WARNING)
	{
		std::cout << "\033[" << opt << ";33m" << message << "\033[0m" << std::endl;
	}
	else if (type == M_ERROR)
	{
		std::cout << "\033[" << opt << ";31m" << message << "\033[0m" << std::endl;
	}
	else
	{
		std::cout << message << std::endl;
	}
}

#endif




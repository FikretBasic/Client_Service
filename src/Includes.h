/*
 * Includes.h
 *
 *  Created on: 7 Feb 2018
 *      Author: its2016
 */

#pragma once

#ifndef INCLUDES_H_
#define INCLUDES_H_

#if defined(_WIN32)

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#define HAVE_STRUCT_TIMESPEC

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <process.h>
#include <conio.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#endif

#include <stdio.h>
#include <cstddef>
#include <stdint.h>
#include <iostream>
#include <pthread.h>
#include <string>
#include <sstream>
#include <ctime>
#include <iterator>

#include "systemc.h"

#include "Socket_Classes/SocketAPI.h"
#include "tools/Stylist.h"
#include "tools/HelperFunctions.h"
#include "tools/SimplePacket.h"


#if defined(_WIN32)
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)  
#define bcopy(b1,b2,len) (memmove((b2), (b1), (len)), (void) 0)

#define strcpy(d,s) (strcpy_s(d,s))
#define strcat(d,s) (strcat_s(d,s))
#endif

#if !defined (_WINSOCKAPI_)
typedef int SOCKET;
#define INVALID_HANDLE_VALUE -1
#endif /* _WINSOCKAPI_ */


/**
 * A template global function used for transforming any data type to string.
 * \param t - data to be handled
 * \return result string
 */
template <typename T>
std::string Str( const T & t ) {
   std::ostringstream os;
   os << t;
   return os.str();
}

#endif /* INCLUDES_H_ */

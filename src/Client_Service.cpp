//============================================================================
// Name        : Client_Service.cpp
// Author      : Fikret
// Version     :
// Copyright   : &copy Fikret Basic - TU Graz All Rights Reserved
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "Includes.h"
#include "Socket_Classes/NETAddr.h"
#include "Socket_Classes/SOCKETLine.h"
#include "Socket_Classes/SOCKETHandler.h"


#if defined(_WIN32)
/**
* \brief Initial setup for start of the Socket communication for Windows
*/
void initializeSocketWin();
#endif

int sc_main(int argc, char* argv[]) {

	std::cout << "\n\t\t\t>>> Client Initialisation <<<" << std::endl << std::endl;

	const char* hostaddr;
	const char* port;
	uint16_t connectionCode;

	if (argc == 1)
	{
		hostaddr = NULL;
		port = "8888";
		connectionCode = 0;
	}
	else if (argc == 2)
	{
		hostaddr = argv[1];
		port = "8888";
		connectionCode = 0;
	}
	else if (argc == 3)
	{
		hostaddr = argv[1];
		port = argv[2];
		connectionCode = 0;
	}
	else if (argc == 4)
	{
		hostaddr = argv[1];
		port = argv[2];
		connectionCode = (uint16_t)atoi(argv[3]);
	}
	else
	{
		Stylist::instance()->printMessage("[STATUS] Incorrect use of the program -- Please follow this guideline:\n"
				"(*) Use the program WITHOUT the arguments (local host address is used with port 8888, connection Code with 0)\n"
				"(*) Use the program WITH 1. (host address) or/and 2. (port number) or/and 3. (connection Code) argument", M_ERROR);

		exit(1);
	}

	#if defined(_WIN32)
		initializeSocketWin();
	#endif

	// =================== SIMULATION SETUP ===================

	// "counter" or "smart_sensor" currently
	simc::Simulation *sim = factoryMethodSimulation(connectionCode);
	sim->initializeSimulation();
	sim->setTraceName(connectionCode);
	sim->processTraceOptions();

	simc::SimulationHandler *simHandler = factoryMethodSimulationHandler(connectionCode);
	simHandler->setSimulation(sim);

	// =================== SIMULATION SETUP ===================

	server::NET_Addr *addr = new server::NET_Addr(hostaddr, port);
	server::SOCKET_Line socketInterface(addr, connectionCode);

	server::SOCKET_Handler socketProcess(socketInterface, simHandler);
	socketProcess.startActivity("simulation");

	sim->closeTrace();

	delete addr;
	delete simHandler;

	return 0;
}

#if defined(_WIN32)
void initializeSocketWin()
{
	WSADATA wsaData;
	int res;

	// Initialize Winsock
	res = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (res != 0) {
		printf("WSAStartup failed with error: %d\n", res);
		exit(1);
	}
}
#endif
